package com.drhoffmannstoolsdataloggerreader;

/* USBDetector.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * =============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details.
 */ 


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.widget.Toast;

/*
 * Dieser BroadcastReceiver bekommt die Intents mit, welche sagen, wenn ein USB-Device abgezogen
 * worden ist. Beim Einstecken geht es leider nicht, aber dann wird schon die 
 * Haupt-Activity mit OnResume gestartet.
 * */

public class USBDetector extends BroadcastReceiver {
  public static USBDataloggerreaderActivity god=null;
  @Override
  public void onReceive(Context context, Intent intent) {
    String action = intent.getAction();
    if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) { 	       
      Toast.makeText(context, "DL-Action: " + action, Toast.LENGTH_LONG).show();
      if(god!=null) god.usb_detached(intent);
    } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {  	      
      Toast.makeText(context, "DL-Action: " + action, Toast.LENGTH_LONG).show();
      if(god!=null) god.usb_attached(intent);
    } else if (Intent.ACTION_MEDIA_MOUNTED.equals(action)) {
      String mountPath = intent.getDataString();
      if(mountPath==null) mountPath="<ERROR>";
      Toast.makeText(context, "DL-Action: " + action+ " Path=" + mountPath, Toast.LENGTH_LONG).show();
      if(god!=null) god.usb_storage_attached(intent);
    } else if (Intent.ACTION_MEDIA_UNMOUNTED.equals(action)) {
      String mountPath = intent.getDataString();
      if(mountPath==null) mountPath="<ERROR>";
      Toast.makeText(context, "DL-Action: " + action+ " Path=" + mountPath, Toast.LENGTH_LONG).show();
      if(god!=null) god.usb_storage_detached(intent);
    }
    else Toast.makeText(context, "DL-Action: " + action, Toast.LENGTH_LONG).show();
  }
}
