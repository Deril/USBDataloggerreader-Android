package com.drhoffmannstoolsdataloggerreader;

/* Freetec.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Calendar;
import java.util.Date;

import android.util.Log;


public class Freetec {
  private static final String TAG = "FREETEC";
  private static String loggerstatus="FreeTec/black logger is not fully supported yet!";

  private static final int CMD_READMEM=  0xa1; 
  private static final int CMD_WRITEMEM= 0xa0;  
  private static final int CMD_READREG=  0xa3; 
  private static final int CMD_WRITEREG= 0xa2;  

  public static int chk_conf(LoggerConfig config, int loggertype) {
    String n=config.getname();
    if(n.length()==0) return Error.ERR_CNONAME;
    if(n.length()>16) return Error.ERR_CNAME;
    if(config.num_data_conf>16320 || config.num_data_conf<0) return Error.ERR_NUMDATA;
    if(config.set_interval>255*60 ||config.set_interval<=0) return Error.ERR_INTERVAL;
    return Error.OK;
  }


  public static byte[] build_conf(Logger logger) {
    byte cbuf[];
    ByteBuffer mes;
    cbuf=read_configblock(logger);
    final LoggerConfig config=logger.config;
    mes=ByteBuffer.allocate(cbuf.length);
    mes.position(0);
    mes.put(cbuf);
    mes.position(0);
    mes.order(ByteOrder.BIG_ENDIAN);
    if(logger.config.set_interval<60) {
      Log.d(TAG,"Config WARNING: Sample rate set to 1 Minute.");
      logger.config.set_interval=60;
    }

    /* Speicherbelegung: 
       Die Orginalsoftware kann auch Datumsformat setzen, 4 Zeitsegmente aktivieren.
    
    00: 0xff          // Temperatureinheit  00=Fahrenheit, 0xff = Celsius
    01: 0x01          // Interval in Minuten
    02: xx xx         // Datenzaehler (Pages) also Blocknummern, fängt bei 1 an (0 würde den configblock überschreiben)
    
    05: 0xff          // 0 = Datumsformat MD, 0xff = Datumsformat DM
    06:  xx xx        // Version
    
    08: xx xx xx xx xx xx xx xx   // 8 Bytes fuer Namen / Seriennummer
    
    10: 0x55          // Latch wechselt auf 0xff wenn übernommen
    11: xx xx xx xx xx xx // Datum und Uhrzeit
    
    
    20: 0xff          // Latch 
    
    30: ff ff ff         // Segment 1
    33: xx xx xx xx xx xx // Datum und Uhrzeit
    39: ff ff ff ff aa d3 74     // ??
    40: ff ff ff ff ff     // Segment 2 Startzeit 
    45  ff ff ff ff ff     // Segment 2 Endzeit
    4a: ff ff ff aa d6 74
    50: ff ff ff ff ff ff ff ff// Segment 3
    58: ff ff ff ff ff aa d6 74
    60: ff ff ff ff ff ff ff ff// Segment 4
    68: ff ff ff ff ff aa d6 74
    

    80: ff ff         // Hier merken wir uns die Adresse des ersten Wertes
    82: xx xx xx xx xx xx   // Datum und Uhrzeit bei Logger-Start
    88: xx xx               // Max Anzahl Daten zu nehmen

   100: Datenbereich bis 0xffff
    
    */



    if(logger.config.temp_unit==Logger.UNIT_F) mes.put(0,(byte)0x00);
    else mes.put(0,(byte)0xff);
    mes.put(1,(byte) (config.set_interval/60));
    if(logger.config.date_format==Logger.DATEFORMAT_MMDDYYYY || 
       logger.config.date_format==Logger.DATEFORMAT_YYYYMMDD) 
         mes.put(5,(byte)0x00);
    else mes.put(5,(byte)0xff);

    boolean flag=false;
    char a;
    for(int i=0;i<16;i++) {
      if(i<config.name.length) a=config.name[i];
      else a=' ';
      if(!((a>='0' && a<='9') || (a>='a' && a<='f') || (a>='A' && a<='F'))) flag=true;
    }
    if(flag) {
      for(int i=0;i<8;i++) {
    	boolean flag2=false;
    	if(i<config.name.length && !flag2) a=config.name[i];
    	else a=' ';
    	if(a==0) {
    	  a=' ';
    	  flag2=true;
    	}
    	mes.put(8+i,(byte)a);
      }
    } else {
      int b,c;
      for(int i=0;i<8;i++) {
        b=config.name[2*i];
        if(b>=48 && b<=57) b-=48;
        else if(b>=65 && b<=70) b-=55;
        else if(b>=97 && b<=102) b-=87;
        b=b*16;
        c=config.name[2*i+1];
        if(c>=48 && c<=57) c-=48;
        else if(c>=65 && c<=70) c-=55;
        else if(c>=97 && c<=102) c-=87;
        mes.put(8+i,(byte)(b+c));
      }
    }

    /*Zeit und Datum, wird bei naechster Datennahme gesetzt.*/

    mes.put(0x10,(byte)0x55);	/*Daran kann man erkennen, wann gelatcht wird*/
    mes.put(0x20,(byte)0xff);	/*Daran kann man erkennen, wann gelatcht wird*/
    mes.put(0x11,(byte)(config.time_year-2000)); 
    mes.put(0x12,(byte)(config.time_mon)); 
    mes.put(0x13,(byte)(config.time_mday)); 
    mes.put(0x14,(byte)(config.time_hour)); 
    mes.put(0x15,(byte)(config.time_min)); 
    mes.put(0x16,(byte)(config.time_sec)); 

    if(logger.do_repair) {
      mes.put(0x88,(byte)0xff); 
      mes.put(0x89,(byte)0xff);
    } else {
      mes.put(0x88,(byte)((config.num_data_conf&0xff00)>>8)); 
      mes.put(0x89,(byte)(config.num_data_conf&0xff)); 
    }
    if(logger.do_repair) {
      /* Daten Zaehler zurücksetzen */
      mes.put(0x02,(byte)0x01);
      mes.put(0x03,(byte)0x01);
    }
    return mes.array();
  }

  /* Read (32 Bytes) at adr from Memory Bank bank of the logger.
     A bank is 256 bytes.  
   */

  private static byte[] read_memory(Logger logger,int bank,int adr) {
    if(logger.do_simulate) return(logger.simulator.readBytes(bank<<8+(adr&0xff), 32));
    byte cbuf[];
    byte mem[]=new byte[32];
    logger.sendHIDCommand((byte)CMD_READMEM,bank,adr,0);
    cbuf=logger.receiveHID();
    for(int i=0;i<8;i++) mem[i]=cbuf[i];
    cbuf=logger.receiveHID();
    for(int i=0;i<8;i++) mem[i+8]=cbuf[i];
    cbuf=logger.receiveHID();
    for(int i=0;i<8;i++) mem[i+16]=cbuf[i];
    cbuf=logger.receiveHID();
    for(int i=0;i<8;i++) mem[i+24]=cbuf[i];
    return mem;
  }


  /* Write (32 Bytes) at adr to Memory Bank of the logger.
     A bank is 256 bytes.
   */

  private static byte[] write_memory(Logger logger,int bank,int adr,byte [] data) {
    if(logger.do_simulate) {
      logger.simulator.writeBytes(bank<<8+(adr&0xff), data);
      return(Simulator.HIDOK);
    }
    byte cbuf[]=new byte[8];
    logger.sendHIDCommand((byte)CMD_WRITEMEM,bank,adr,0);
    for(int i=0;i<8;i++) cbuf[i]=data[i];
    logger.sendHIDData(cbuf);
    for(int i=0;i<8;i++) cbuf[i]=data[i+8];
    logger.sendHIDData(cbuf);
    for(int i=0;i<8;i++) cbuf[i]=data[i+16];
    logger.sendHIDData(cbuf);
    for(int i=0;i<8;i++) cbuf[i]=data[i+24];
    logger.sendHIDData(cbuf);
    cbuf=logger.receiveHID();
    return cbuf;
  }


  /* geht nicht*/

  private static byte[] read_register(Logger logger,int nr) {
    byte cbuf[];
    if(logger.do_simulate) {
      cbuf=new byte[8];
      cbuf[0]=logger.simulator.readByte(nr);
    } else {
      logger.sendHIDCommand((byte)CMD_READREG,0,nr,0);
      cbuf=logger.receiveHID();
    }
    return cbuf;
  }

  /* Write a single byte to loggers memory (bank 0 ?)*/
  private static byte[] write_register(Logger logger,int nr,int data) {
    byte cbuf[];
    if(logger.do_simulate) {
      logger.simulator.writeByte(nr, (byte)data);
      cbuf=Simulator.HIDOK;
    } else {
      logger.sendHIDCommand((byte)CMD_WRITEREG,0,nr,data);
      cbuf=logger.receiveHID();
    }
    return cbuf;
  }

  /* Read a whole block (bank) of 256 Bytes from Logger */
  public static byte[] read_block(Logger logger,int n) {
    byte cbuf[]=new byte[256];
    logger.log2comproto("Read Block #"+n,1);
    for(int i=0;i<0x100;i+=0x20) {
      byte mem[]=read_memory(logger,n, i);
      for(int j=0;j<32;j++) cbuf[i+j]=mem[j];
      logger.log2comproto(Logger.bytearray2string(mem), 2);
    }
    return cbuf;
  }

  public static byte[] read_configblock(Logger logger) {
    return read_block(logger,0);
  }

  private static long read_clock(Logger logger) {
    byte mem[];
    write_register(logger,0x20,0xff);
    mem=read_memory(logger,0, 0x20);
    while(mem[0x0]==(byte)0xff) {
      Log.d(TAG,"Waiting for sync...");
      try {Thread.sleep(500);}
      catch (InterruptedException e1) {;}
      mem=read_memory(logger,0, 0x20);
    }
    Calendar cal = Calendar.getInstance();
    cal.set(2000+mem[1],mem[2]-1, mem[3], mem[4], mem[5], mem[6]);
    return(cal.getTimeInMillis()/1000);
  }

  private static long write_clock(Logger logger,long dt) {
    byte mem[];
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(dt*1000);
    write_register(logger,0x11,(byte)(cal.getTime().getYear()+1900-2000));
    write_register(logger,0x12,(byte)(cal.getTime().getMonth()+1));
    write_register(logger,0x13,(byte)(cal.getTime().getDate()));
    write_register(logger,0x14,(byte)(cal.getTime().getHours()));
    write_register(logger,0x15,(byte)(cal.getTime().getMinutes()));
    write_register(logger,0x16,(byte)(cal.getTime().getSeconds()));
    write_register(logger,0x10,0x55);

    mem=read_memory(logger,0, 0x10);
    while(mem[0x0]!=(byte)0xff) {
      Log.d(TAG,"Waiting for sync...");
      try { Thread.sleep(300);} 
      catch (InterruptedException e1) { ; }
      mem=read_memory(logger,0, 0x10);
    }
    return(new Date().getTime()/1000);
  }

  /*Finds out, when the dataloggers internal sync takes place and sets the internla clock
   * to system.time, it is synchronized 
   * with the system time (to seconds)
   * This procedure can take up to interval minutes!*/
  private static long sync_clock(Logger logger) {
    Date dt=new Date();
    int count=0;
    byte mem[]=read_memory(logger,0, 0x10);
    logger.updateProgress(0);
    mem[0]=0x55; /*Daran kann man erkennen, wann gelatcht wird*/
    mem[0x10]=(byte)0xff; /*gleichzeitig ein read machen*/
    while(mem[0x10]==(byte)0xff) {
      dt=new Date();
      mem[1]=(byte)(dt.getYear()+1900-2000); 
      mem[2]=(byte)(dt.getMonth()+1); 
      mem[3]=(byte)(dt.getDate()); 
      mem[4]=(byte)(dt.getHours()); 
      mem[5]=(byte)(dt.getMinutes()); 
      mem[6]=(byte)(dt.getSeconds()); 
      write_memory(logger,0,0x10,mem);
      write_register(logger,0x20,0xff);
      write_register(logger,0x10,0x55);
      logger.updateStatus("Setting the clock..."+dt.toLocaleString(), 2);
      logger.updateProgress(count/2);
      try { Thread.sleep(500);}
      catch (InterruptedException e1) { ; }
      mem=read_memory(logger,0, 0x10);
      if(count<200) count++;
    }
    logger.updateStatus("Clock set to "+dt.toLocaleString(), 0);
    logger.updateProgress(100);
    return(dt.getTime());
  }

  private static long sync_timestamp(Logger logger, int adr) {
    byte mem2[];
    Date dt=new Date();
    int count=0;
    //	write_register(logger,0x20,0xff);  /* Lese Uhr */
    logger.updateProgress(0);
    mem2=read_memory(logger,(adr&0xff00)>>8,(adr&0xff));
    while(mem2[0x0]==(byte)0xff) {
      Log.d(TAG,"*Waiting for sync...");
      dt=new Date();
      logger.updateStatus("Synchronizing Timestamps..."+dt.toLocaleString(), 2);
      try { Thread.sleep(500); }
      catch (InterruptedException e1) { ; }
      logger.updateProgress(count/2);
      if(count<200) count++;
      mem2=read_memory(logger,(adr&0xff00)>>8,(adr&0xff));
    }
    dt=new Date();
    logger.updateStatus("Timestamp sync was on "+dt.toLocaleString(), 0);
    Log.d(TAG,"Value Sync was on: "+dt.toString());
    logger.updateProgress(100);
    return(dt.getTime()/1000);
  } 

  /*Schätzt den Zeitpunkt des nächsten Datenpunktes ab anhand eines alten
   * Zeitpunktes und dem alten Interval.*/


  private static long estimate_timestamp(Logger logger, int oi) {
    byte[] mem=read_memory(logger,0, 0x80);
    Log.d(TAG,Logger.bytearray2string(mem));
    Calendar cal = Calendar.getInstance();
    cal.set(mem[2]+2000, mem[3]-1, 
  	  	    (int)mem[4], 
  	  	    (int)mem[5], 
  	  	    (int)mem[6], 
  	  	    (int)mem[7]);
    long ss=cal.getTimeInMillis();
    Date dt=new Date();
    Log.d(TAG,""+ss+"/"+dt.getTime());
    long dd=dt.getTime()-ss;
    dd/=1000;
    Log.d(TAG,""+dd+"/"+oi);
    long fs=(dd/oi);
    long last=ss+fs*oi*1000;
    // long next=last+logger.config.got_interval*1000;
    long next=last+oi*1000; /* muss oi sein, da erst beim naechsten Punkt auf das neue
    interval umgeschaltet wird.*/
    dt.setTime(next);
    Log.d(TAG,"Next data timestamp estimated to "+dt.toLocaleString());
    logger.updateStatus("Next data timestamp estimated to "+dt.toLocaleString(), 0);
    return(next/1000); 
  }

  /*Liefert die Adresse des ersten Freien Bereichs zuruck (also
   * die Adresse, wo der nächste Wert gespeichert wird.)
   * oder -1 für Fehler
   * Ausserdem der letzte gespeicherte Wert
   * oder -1
   * */

  private static int find_last_adr_value(Logger logger) {
    byte mem[]=read_memory(logger,0, 0);
    int sector=(mem[2]&0xff);

    int i,j=0;
    int adr=-1;
    for(i=0;i<0x100;i+=0x20) {
      mem=read_memory(logger,sector,i);

      for(j=0;j<32;j+=4) {
    	if((mem[j]&0xff)==0xff) break;
    	else logger.config.rawinputreading=(mem[j]&0xff)*256*256*256+
    	  		(mem[j+1]&0xff)*256*256+
    	  		(mem[j+2]&0xff)*256+
    	  		(mem[j+3]&0xff);
      }
      if(j<32) break;
      j=0;
    }
    if(i==0x100) Log.d(TAG,"WARNING: full sektor...");
    adr=sector*256+i+j;
    return adr;
  }

  private static void calculate_start_adr(Logger logger) {
    Calendar cal = Calendar.getInstance();
    cal.set(logger.config.time_year, (int)(logger.config.time_mon&0xff)-1, 
      (int)(logger.config.time_mday&0xff), 
      (int)(logger.config.time_hour&0xff), 
      (int)(logger.config.time_min&0xff), 
      (int)(logger.config.time_sec&0xff));
    long ss=cal.getTimeInMillis()/1000;
    Log.d(TAG,"Config Time: :"+cal.getTime().toLocaleString());
    Log.d(TAG,"Config Time: :"+logger.config.time_year+":"+logger.config.time_mon);
    ss=logger.config.timestamp-ss;
    Log.d(TAG,"Time since = "+ss+" Seconds.");
    int anz=(int)(ss/logger.config.got_interval)+1;
    Log.d(TAG,"Anz = "+anz+" .");
    Log.d(TAG,"Fehlende Sekunden:"+ss%logger.config.got_interval);
    Log.d(TAG,"Zum Vergleich: num_data_rec:"+logger.config.num_data_rec);

    /* Hier kann man rollovercount berechnen.*/
    logger.config.rollover_count=4*anz/(0x10000-0x100);
    if(logger.config.rollover_count!=0) loggerstatus+="rollover: "+logger.config.rollover_count+" ";

    /* Absolutes maximum der Auslesbaren Daten für diesen Logger berechnen*/
    int maxdata=(0x10000-0x100-0x100+(logger.config.config_end&0xff))/4;
    Log.d(TAG,"Es können maximal ausgelesen werden:"+maxdata);

    /* Evtl. auf kleineres num_data_conf reagieren */

    if(anz>logger.config.num_data_conf) anz=logger.config.num_data_conf;
    if(anz>maxdata) anz=maxdata;

    /*Letztlich mussen die Zeitwerte in config korrigiert werden.
     * genauso die Auslese-Startadresse.*/
    cal.setTimeInMillis(1000*(logger.config.timestamp-
  	  	    (ss%logger.config.got_interval)-(anz-1)*logger.config.got_interval));
    Log.d("TAG","Neue Anfangszeit: "+cal.getTime().toLocaleString());
    if(logger.config.num_data_rec!=anz) 
  	    logger.updateMessage("Read "+anz+" instead of "+logger.config.num_data_rec+" data points."+
  	  		    "Begin of Data is: "+cal.getTime().toLocaleString()+" next data in "+(logger.config.got_interval-(ss%logger.config.got_interval))+" seconds.", 2);
    else 
  	    logger.updateMessage("Begin of Data is: "+
  	  		    cal.getTime().toLocaleString()+" next data in "+(logger.config.got_interval-(ss%logger.config.got_interval))+" seconds.", 1);

    Date dt=cal.getTime();
    logger.config.time_year=dt.getYear()+1900;
    logger.config.time_mon=(byte)(dt.getMonth()+1);
    logger.config.time_mday=(byte)dt.getDate();
    logger.config.time_hour=(byte)dt.getHours();
    logger.config.time_min=(byte)dt.getMinutes();
    logger.config.time_sec=(byte)dt.getSeconds();

    /*neue Auslese-Adresse berechnen:*/
    Log.d(TAG,"Start: 0x"+String.format("%04x", logger.config.config_begin)+" End: 0x"+String.format("%04x", logger.config.config_end));
    int endadr=logger.config.config_end;
    int startadr=endadr-anz*4;
    if(startadr<0x100) startadr+=0x10000-0x100;
    Log.d(TAG,"Start: 0x"+String.format("%04x", startadr)+" End: 0x"+String.format("%04x", logger.config.config_end));

    if(logger.config.config_begin!=startadr) loggerstatus+="adr-corr: "+(logger.config.config_begin-startadr)/4+" ";
    logger.config.config_begin=startadr;

    // hier ggf. numdatarec anpassen!
    logger.config.num_data_rec=anz;
  }

  public static int write_configblock(byte[] cbuf,Logger logger) {
    int ret=Error.OK;
    Log.d(TAG,"writeconfigblock...");
    if(logger.isconnected) {
      /*Altes Interval für später merken*/
      byte[] mem=read_memory(logger,0, 0);
      int old_interval=(mem[1]&0xff)*60;
      /**/

      /* ID/Name setzen*/
      for(int i=8;i<16;i++) write_register(logger,i,cbuf[i]);
      /* Zeit und Datum in Latch*/
      /* Machen wir hier lieber nicht, da das die Uhr nur ungenauer macht.*/
      // for(int i=16;i<23;i++) write_register(logger,i,cbuf[i]);
      // write_register(logger,0x20,cbuf[0x20]); /*Damit man die Übernahme erkennen kann.*/
      if(logger.do_repair) {
  	write_register(logger,0x02,cbuf[2]);  /*Datenzaehler zuruecksetzen*/
  	write_register(logger,0x03,cbuf[3]);  /*Datenzaehler zuruecksetzen*/
      }

      /* Wir suchen hier nach der Adresse des
       * ersten neuen Wertes und
       * merken uns die zugehoerige Zeit.
       * 
       * */
      int adr=find_last_adr_value(logger);
      if(adr>=0x10000) adr=0x100;
      Log.d(TAG,"Letzter Wert bei "+adr);
      if(logger.do_repair) {
  	write_register(logger,0x80,0xff);
  	write_register(logger,0x81,0xff);
      } else {
  	write_register(logger,0x80,(adr>>8));
  	write_register(logger,0x81,(adr&0xff));
      }
      
      /* Interval setzen und Fahrenheitflag*/

      write_register(logger,0x00,cbuf[0]);   // Temperatur-Unit 
      write_register(logger,0x05,cbuf[5]);   // Datumsformat
      write_register(logger,0x01,cbuf[1]);   // Interval 


      /* Hier schauen wir, ob die langwierige Zeitstempel
       * Synchronisation nötig ist. 
       * Wir machen Sie einfach nur bei Interval=1 Minute*/
      long a=-1;
      if(old_interval<120) {
  	logger.updateStatus("Synchronizing Timestamps...", 2);
  	a=sync_timestamp(logger,adr);
      } else {
  	/*Ansonsten rechnen wir aus, wann der nächte Meßwert
  	 * erscheinen sollte: */
  	logger.updateStatus("Estimate Timestamps...", 2);
  	a=estimate_timestamp(logger,old_interval);
      }
      //	      write_register(logger,0x80,(0xff));
      //	      write_register(logger,0x81,(0xff));
      Date dt = new Date();

      dt.setTime(1000*a);
      write_register(logger,0x82,dt.getYear()+1900-2000);
      write_register(logger,0x83,(byte) (dt.getMonth()+1));
      write_register(logger,0x84,(byte)dt.getDate());
      write_register(logger,0x85,(byte)dt.getHours());
      write_register(logger,0x86,(byte)dt.getMinutes());
      write_register(logger,0x87,(byte)dt.getSeconds());

      /*Num data config schreiben*/
      write_register(logger,0x88,cbuf[0x88]);
      write_register(logger,0x89,cbuf[0x89]);
      if(logger.config.set_interval<120) {
  	logger.updateStatus("Setting the clock...", 2);
  	sync_clock(logger);
      }
      logger.do_repair=false;
    } else {
      Log.d(TAG, "connection problem ");
      ret=Error.ERR;
    }
    return(ret);
  }

  /* Standardprocedure "READ LOGGER" for this loggertype...*/	

  public static int get_config(Logger logger) {
    byte cbuf[]=read_configblock(logger);
    if(cbuf==null) return Error.ERR;
    LoggerConfig config=logger.config;
    Date dt = new Date();
    int ret=Error.OK;
    config.timestamp=dt.getTime()/1000;
    config.configbuf=cbuf.clone();
    config.cbufsize=cbuf.length;
    if(cbuf.length<256) return Error.ERR_BAD;
    loggerstatus="Config OK: ";
    config.time_offset=0;
    config.flag_bits=0;
    config.block_type=0;
    config.block_cmd=0;
    config.templimit_high=60;
    config.templimit_low=-40;
    config.calibration_Mvalue=0;
    config.calibration_Cvalue=0;
    config.humilimit_high=100;
    config.humilimit_low=0;
    config.rollover_count=0;
    config.scaling_factor=(float)1.0;
    config.flag_bits2=0;
    config.mglobal_message="";
    ByteBuffer buffer = ByteBuffer.allocate(cbuf.length);
    buffer.position(0);
    buffer.put(cbuf);
    buffer.position(0);
    buffer.order(ByteOrder.BIG_ENDIAN);
      logger.packet_size=8;
      logger.memory_size=0xfe00;
      
      int i;
      boolean flag=false;
      for(i=0;i<8;i++) {
        if(!( ((cbuf[8+i]&255)>='A' && (cbuf[8+i]&255)<='Z') || 
              ((cbuf[8+i]&255)>='a' && (cbuf[8+i]&255)<='z') ||
              ((cbuf[8+i]&255)>='0' && (cbuf[8+i]&255)<='9') || 
              (cbuf[8+i]&255)==' ')) flag=true;
      }
      if(flag) {
        for(i=0;i<8;i++) {
          String a=String.format("%02x",(cbuf[8+i]&255));
          config.name[2*i]=a.charAt(0);
          config.name[2*i+1]=a.charAt(1);
        }
      } else {
        for(i=0;i<8;i++)  		config.name[i]=(char)(cbuf[8+i]&255);
        for(i=8;i<config.name.length;i++) config.name[i]=(char)0;
      }
      if((buffer.get(0x80)&0xff)==0xff && (buffer.get(0x81)&0xff)==0xff) { /* wurde nach Reset noch nicht konfiguriert*/
	config.time_hour=buffer.get(36);
	config.time_min=buffer.get(37);
	config.time_sec=buffer.get(38);
	config.time_mday=buffer.get(35);
	config.time_mon=buffer.get(34);
	config.time_year=buffer.get(33)+2000;
	config.config_begin=0x100;
	config.num_data_conf=(0x10000-0x100)/4;
      } else {
	config.time_hour=buffer.get(0x85);
	config.time_min=buffer.get(0x86);
	config.time_sec=buffer.get(0x87);
	config.time_mday=buffer.get(0x84);
	config.time_mon=buffer.get(0x83);
	config.time_year=buffer.get(0x82)+2000;
	config.config_begin=((buffer.get(0x80)&0xff)<<8)+(buffer.get(0x81)&0xff);
	config.num_data_conf=((buffer.get(0x88)&0xff)<<8)+(buffer.get(0x89)&0xff);
	if(config.num_data_conf>(0x10000-0x100)/4 || config.num_data_conf<=0)
	  config.num_data_conf=(0x10000-0x100)/4;
      }
      config.config_end=(buffer.get(2)<<8);
      config.got_interval=60*(buffer.get(1)&0xff);
      config.set_interval=config.got_interval;
      if((buffer.get(3)&0xff)==0xff) loggerstatus+="memory full. ";
      else if(buffer.get(2)!=buffer.get(3)) {
	logger.log2comproto("config: different page values: "+buffer.get(2)+"-"+buffer.get(3),0);
	loggerstatus+="page anomaly:"+buffer.get(2)+"-"+buffer.get(3)+" ";
      }
      //	config.num_data_rec=buffer.get(2)*64;

      if(buffer.get(0)!=0) config.temp_unit=Logger.UNIT_C;
      else config.temp_unit=Logger.UNIT_F;
      
      if(buffer.get(5)!=0) config.date_format=Logger.DATEFORMAT_DDMMYYYY;
      else config.date_format=Logger.DATEFORMAT_MMDDYYYY;
      
      String a=String.format("%02x%02x",buffer.get(6),buffer.get(7));;
      for(i=0;i<4;i++) config.version[i]=(char)a.charAt(i);
      config.serial_number=buffer.getInt(8);
      int adr=find_last_adr_value(logger);
      config.config_end=adr;
      config.num_data_rec=(adr-config.config_begin)/4;
      if(config.num_data_rec<0) {
	config.num_data_rec=(0x10000-config.config_begin+adr-0x100)/4;
      }
      calculate_start_adr(logger);
    logger.isreadconfig=true;
    return ret;
  }


  public static double raw2temp(int raw) {
    raw=(raw&0xffff0000)>>16;
    int sign=(raw&0x8000)>>15;
    raw=(raw&0x3ff);
    if(sign==0) return((double)(raw-400)/10);
    else return(-(double)(raw-400)/10);
  }

  /*Read one datapacket from Freetec black */
  private static int getdatapacket(Logger logger, int blk, int start, int stop) {
    Log.d(TAG,"getdatapacket: "+blk);
    final LoggerData data=logger.data;
    byte buf[]=read_block(logger,blk);
    int i,interva,hi,lo,sign=0;
    short t=-1;
    for(i=0;i<buf.length/4;i++) {
      //  if((buf[i*4+1]&0xff)==0xff && (buf[i*4+2]&0xff)==0xff) break; 
      interva=(buf[i*4+3]&0xff);
      sign=(buf[i*4]&0x80)>>7;
      hi=(buf[i*4]&0x3);
      lo=(buf[i*4+1]&0xff);
      if(sign==0) t=(short)(hi*256+lo-400);
      else t=(short)-(hi*256+lo-400);
      if(interva*60!=logger.config.got_interval) 
  	  Log.d(TAG,"Config WARNING: Data has different interval."+interva);
      if(i*4>=start && i*4<stop) data.add(t,(short) (buf[i*4+2]&0xff));
    }
    return Error.OK;
  }

  /* Read one datapacket from Freetec white */

  private static int getdatapacket_white(Logger logger, int idx, int anz) {
    if(idx>=0 && idx<324 && anz>0 && anz<=64) {
      byte buf[]=new byte[64*3];
      logger.log2comproto("Read Data Chunk #"+idx,1);
      int n;
      for(int i=0;i<64*3;i+=0x20) {
        n=0xd00+idx*3*64+i;
        byte mem[]=read_memory(logger,((n&0xff00)>>8),(n&0xff));
        for(int j=0;j<32;j++) buf[i+j]=mem[j];
        logger.log2comproto(Logger.bytearray2string(mem), 2);
      }
      short t=-1;
      short h=-1;
      for(int i=0;i<anz;i++) {
        h=(short)(buf[i*3+0]&0xff);
        t=(short)(((buf[i*3+1]&0xff)<<8)+(buf[i*3+2]&0xff));
        logger.data.add(t,h);
      }
    }
    return Error.OK;
  }

  public static int get_data(Logger logger) {
    int i,q=0,c=1,p1=1,p2=1,p3=0,p4=0;
    boolean do_all=((logger.config.flag_bits2&1)==0);
    final LoggerData data=logger.data;
    final LoggerConfig config=logger.config;
      data.set_calibration((float)0.1,0, 1, 0);
      logger.updateStatus(0);
      p1=(config.config_begin>>8);
      p3=(config.config_begin&0xff);
      p2=(config.config_end>>8);
      p4=(config.config_end&0xff);
      /* Hier jetzt den Wahren Anfang finden, errechnet aus Anfangsdatum, interval und
       * numconfig.   */

      if(do_all) { /*Wenn alle Daten gelesen werden sollen*/
        p3=0;
        p1=p2+1;
        if(p1>=0x100) p1=0x1;
      }

      /* ***** */
      if(p1<=p2) {
        c=p2-p1+1;
        for(i=0;i<c;i++) {
	  if(c==1) getdatapacket(logger,p1+i,p3,p4);
	  else if(i==0) getdatapacket(logger,p1+i,p3,0x100);
	  else if(i==c-1) getdatapacket(logger,p1+i,0,p4);
	  else getdatapacket(logger,p1+i,0,0x100);
	  // Update the progress bar
	  logger.updateProgress(100*i/c);
        }
      } else {
        int count=0;
        /* rollover*/
        c=0x100-p1+p2;
        i=p1;
        while(i!=p2) {
	  if(i==p1) {
	    getdatapacket(logger,p1,p3,0x100); /*Erstes von min 2.*/
	    p3=0;
	  } else getdatapacket(logger,i,0,0x100);
	  count++;
	  logger.updateProgress(100*count/c);
	  i++;
	  if(i==0x100) i=1;
	}
	getdatapacket(logger,p2,p3,p4); /*Letztes Paket*/
	logger.updateProgress(100);
      }

    /* Nun Qualitaetscheck der Daten machen.*/
    if(logger.loggertype==Logger.LTYP_TH) {
      for(i=0;i<config.num_data_rec;i++) {
    	if(data.get_temp(i)>100 || data.get_temp(i)<-100 || 
    	  		data.get_rh(i)>100 || data.get_rh(i)<0) q++;
      }
    } else q=0;
    logger.data.quality=q;
    return(q);  
  }


  public static String get_status(Logger logger) {
    String ret="";
    if(logger.isreadconfig) ret=loggerstatus;
    else ret="Config has not yet been read";
    return ret+".";
  }
}
