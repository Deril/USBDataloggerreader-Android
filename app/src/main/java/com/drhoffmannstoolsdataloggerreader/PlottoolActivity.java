package com.drhoffmannstoolsdataloggerreader;

/* PlottoolActivity.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class PlottoolActivity  extends Activity {
	private PlotView plot;
	private static final String TAG = "USBDL Plottool";
	private PlotdataLoader mLoaderTask = null;
	private ProgressDialog pd;
	private static DataContent mdata=null;
	private FileSelect fileselect;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plottool);
		fileselect=new FileSelect(8);
		plot=(PlotView) findViewById(R.id.mainplot); 
		//   plot.setTestData();
		plot.setTimeX(true);
		TextView testTextView = (TextView) findViewById(R.id.dummyTextView);
		float textSize = testTextView.getTextSize();
		plot.setTextsize(textSize);
		Log.d(TAG,"On create. dataisloaded="+(mdata!=null));

		pd = new ProgressDialog(this);
		pd.setIndeterminate(true);
		pd.setCancelable(false);
		pd.setTitle(getResources().getString(R.string.word_loaddata));
		pd.setMessage(getResources().getString(R.string.word_readfiles));
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

		ActionBar actionBar = getActionBar();
		//  actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(getResources().getString(R.string.word_plottooltitle));


		// There are two situations where onCreate is called: (a) a completely new activity;
		// (b) a new activity created as part of a change in orientation.
		// We can tell the difference by looking at the result of getLastNonConfigurationInstance.
		Object retained = getLastNonConfigurationInstance ();
		if (retained == null) {
			// If there is no retained object, no DataLoading is in Progress
			if(mdata==null) {
				fileselect.loadFileList();
				refresh();
			}
		} else {       
			// If there is a task already running, connect it to the this new activity object.
			mLoaderTask= (PlotdataLoader) retained;
			mLoaderTask.resetActivity (this);
			if(mLoaderTask.isAlive()) pd.show();
		}
	}
	@Override
	public void onStart() {
		super.onStart();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		fileselect.setNumFiles(prefs.getInt("fileselect", 8));
		plot.setHistEnable(prefs.getBoolean("histograms", false));
		plot.setGridEnable(prefs.getBoolean("grid", true));
		plot.setEventEnable(prefs.getBoolean("event", false));
		plot.setLabelEnable(prefs.getBoolean("label", false));
		plot.setTPenable(prefs.getBoolean("dewpoint", false));
		if(mLoaderTask==null || !mLoaderTask.isAlive()) {
			if(mdata!=null) updatePlot(mdata);
		}
	}
	//	    @Override
	//	    public void onResume() {
	//	        super.onResume();
	//	    }


	/**
	 * onDestroy
	 * The final call you receive before your activity is destroyed. 
	 * This can happen either because the activity is finishing (someone called finish() on it, 
	 * or because the system is temporarily destroying this instance of the activity to save space. 
	 * You can distinguish between these two scenarios with the isFinishing() method.
	 *
	 */
	@Override
	public void onDestroy () {
		super.onDestroy ();
		Log.d(TAG,"On destroy. isfinishing="+isFinishing ());
		if (isFinishing ()) endBackgroundTasks (true);
	}
	/**
	 * onRetainNonConfigurationInstance
	 * Called by the system, as part of destroying an activity due to a configuration change, 
	 * when it is known that a new instance will immediately be created for the new configuration.
	 * Return an object that can be retrieved with getLastNonConfigurationInstance.
	 *
	 * @return Object - object to be saved
	 */

	public Object onRetainNonConfigurationInstance () {
		if (mLoaderTask != null) {
			mLoaderTask.resetActivity (null);
		}
		return mLoaderTask;
	}
	/**
	 * End any background tasks that might be running and clean up.
	 * 
	 * @param cleanup boolean - true means that any thread objects should be released
	 * @return void
	 */

	private void endBackgroundTasks (boolean cleanup) {
		if(mLoaderTask != null) {
			// If we are cleaning up, it means that the UI is no longer available or will soon be unavailable.
			if(cleanup) {mLoaderTask.disconnect();}
			// Make sure the task is cancelled.
			mLoaderTask.cancel (true);
			// Finish cleanup by removing the reference to the task
			if (cleanup) {
				mLoaderTask = null;
				Log.d(TAG,"endBackgroundTasks: Interrupted and ended task.");
			} else Log.d(TAG,"endBackgroundTasks: Interrupted task.");
		}
	}


	/*This will be called by the background task before doing something*/

	public void prepare_progressdialog() {
		pd.setProgress(0);
		pd.setIndeterminate(false);
		pd.show();
	}

	/*This triggers the load Data thread in the background
	 * */

	public void refresh() {
		PlotdataLoader t = mLoaderTask;
		if (t != null) {
			Log.d(TAG,"need to wait for the previous task to finish.");
		} else {
			/*Korrekturdaten lesen:*/
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
			boolean dopcorr=prefs.getBoolean("pcorr", false);
			float pcorrfactor=prefs.getFloat("pcorr_fakt", (float)0.4);
			float pcorrlatency=prefs.getFloat("pcorr_latenz", (float)7*60);
			boolean lowpassp=prefs.getBoolean("lowpassp", false);
			int lowpassporder=prefs.getInt("lowpassporder",1);
			boolean dotaupunkt=prefs.getBoolean("dewpoint", false);
			int bluecurvetype=prefs.getInt("bluecurvetype",0);


			PlotdataLoader rct = new PlotdataLoader (this, fileselect.mFileList,fileselect.mcheckitems,dotaupunkt,bluecurvetype,
					dopcorr,pcorrfactor,pcorrlatency,lowpassp,lowpassporder);
			mLoaderTask = rct;

			// Start the task. Pass two arguments (integer).  but this is just a
			// mock-up so the values do not really matter.
			rct.execute(4711, 815);
		}
	}


	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.plottoolmenu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId())  {
		case android.R.id.home:
			// This is called when the Home (Up) button is pressed
			// in the Action Bar.
			finish();
			return true;
		case R.id.vdl_options_preferences:
			startActivity(new Intent(this, PreferencesActivity.class));
			return true;
		case R.id.vdl_options_about:
			showDialog(0);
			return true;
		case R.id.vdl_options_calibration:
			startActivity(new Intent(this, CalibrationActivity.class));
			return true;
		case R.id.vdl_options_help:
			showDialog(1);
			return true;
		case R.id.vdl_options_finish:
			finish();
			return true;
		case R.id.vdl_options_refresh:
			refresh();
			return true;
		case R.id.vdl_options_choosefile:
			fileselect=new FileSelect();
			fileselect.loadFileList();
			mdata=null;
			fileselect.fileselector(this).show();
			return true;
		case R.id.menu_savebitmap:
			savebitmap();
			return true;
		default: 
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected Dialog onCreateDialog(final int id) {
		Dialog dialog = null;
		if(id==1) {
			dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.plottoolhelpdialog));
		} else if(id==0) {
			dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.aboutdialog)+
					getResources().getString(R.string.impressum));
		}  else dialog=fileselect.fileselector(this);
		return dialog;
	}

	public void updateProgress(int i) {
		if(i>=0) {
			pd.setProgress(i%1000);
			pd.setMessage(fileselect.mFileList[i/1000]);
		}
		else {
			pd.setProgress(100);
			pd.setMessage(getResources().getString(R.string.word_processing));
		}
	}

	public void loadDataComplete(DataContent data,boolean cancel) {
		if(cancel) Log.d(TAG,"LoadData Task cancelled after " + data.nfiles + " Files. anzdata="+data.anzdata);
		else Log.d(TAG,"LoadData Task ended after " + data.nfiles + " Files. anzdata="+data.anzdata);
		mLoaderTask.disconnect ();
		mLoaderTask = null;
		if(!cancel && data.anzdata>0) mdata=data;
		updatePlot(data);
		pd.dismiss();
	}
	//Assuming the arrays x[],y[],... are already filled

	private void updatePlot(DataContent data) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		boolean fix1=prefs.getBoolean("fixscale1", false);
		float minscale1=prefs.getFloat("minscale1", 0);
		float maxscale1=prefs.getFloat("maxscale1", 0);
		float stepscale1=prefs.getFloat("stepscale1", 0);
		boolean fix2=prefs.getBoolean("fixscale2", false);
		float minscale2=prefs.getFloat("minscale2", 0);
		float maxscale2=prefs.getFloat("maxscale2", 0);
		float stepscale2=prefs.getFloat("stepscale2", 0);
		boolean fix3=prefs.getBoolean("fixscale3", false);
		float minscale3=prefs.getFloat("minscale3", 0);
		float maxscale3=prefs.getFloat("maxscale3", 0);
		float stepscale3=prefs.getFloat("stepscale3", 0);		    	
		plot.setData(data);
		plot.setAutoRangeX();
		if(fix1) plot.setYRange(minscale1,maxscale1);
		else plot.setAutoRangeY();
		if(fix2) plot.setY2Range(minscale2,maxscale2);
		else plot.setAutoRangeY2();
		if(data.y3enabled) {
			if(fix3) plot.setY3Range(minscale3,maxscale3);
			else plot.setAutoRangeY3();
		}
		plot.setAutoGridX();
		if(fix1) plot.setYGrid(stepscale1);
		else plot.setAutoGridY();
		if(fix2) plot.setY2Grid(stepscale2);
		else plot.setAutoGridY2();
		if(data.y3enabled) {
			if(fix3) plot.setY3Grid(stepscale3);
			else plot.setAutoGridY3();
		}
		plot.postInvalidate();
	}

	public void savebitmap() {
		FileOutputStream fos=null;
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd-HHmmss",Locale.US);

		String filename="Diagramm-"+sdf.format(date)+".png";
		Bitmap bmp;
		try {
			/* Hier jetzt ein externe von aussen lesbares verzeichnis*/
			File dirdata=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
			dirdata.mkdirs();
			File file=new File(dirdata,filename);
			fos=new FileOutputStream(file,false);
			bmp=View2Bitmap(plot);
			bmp.compress(Bitmap.CompressFormat.PNG, 90, fos);
			fos.flush();
			Toast.makeText(getApplicationContext(), "File "+file.getAbsolutePath()+" saved.", Toast.LENGTH_LONG).show();
		} catch (Throwable t) {
			// FilenotFound oder IOException
			Log.e(TAG,"open/save. ",t);
		} finally {
			if(fos!=null) {
				try {
					fos.close();
				} catch (IOException e) {
					Log.e(TAG,"fos.close ",e);
				}
			}
		}
	}

	public static Bitmap View2Bitmap(View v) {
		Log.d(TAG,"width="+v.getLayoutParams().width);
		v.setDrawingCacheEnabled(true);
		Bitmap b= v.getDrawingCache();
		Canvas c = new Canvas(b);
		v.draw(c);
		return b;
	}
}
