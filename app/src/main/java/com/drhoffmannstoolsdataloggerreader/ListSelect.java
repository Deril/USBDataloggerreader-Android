package com.drhoffmannstoolsdataloggerreader;

/* ListSelect.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.util.Arrays;
import java.util.Comparator;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnShowListener;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

public class ListSelect {
  public String[] mList;
  public boolean[] mcheckitems;
  private static final String TAG = ListSelect.class.getSimpleName(); 
  private String mChosen;
  private int mSelected=0;
  private AlertDialog dialog;
  private boolean multichoice=false;


  public ListSelect() {
    clear();
  }
  public ListSelect(boolean mc) {
    multichoice=mc;
    clear();
  }
  private void clear() {
    dialog=null;
  }
  public void setlist(String [] a) {
    mList=a;
    mcheckitems=new boolean[a.length];
    if(mSelected>=a.length) mSelected=a.length-1;
    mChosen=mList[mSelected];
  }
  public void set_default(int n) {
    if(n<mList.length) {
      mSelected=n;
      for(int i=0;i<mList.length;i++) mcheckitems[n]=(i==n);
      mChosen=mList[mSelected];
    }
  }
  public void set_default(String s) {
    if(mList.length>0) {
      for(int i=0;i<mList.length;i++) {
        if(mList[i].equals(s)) {
          mcheckitems[i]=true;
	  mSelected=i;
	} else mcheckitems[i]=false;
      }
    }
  }
  public int get_selected() {
    if(multichoice) {
      if(mList.length>0) {
        for(int i=0;i<mList.length;i++) {
          if(mcheckitems[i]==true) return(i);
        }
      }
      return(-1);
    } else return mSelected;
  }
  public String get_selected_item() {
    if(multichoice) {
      if(mList.length>0) {
        for(int i=0;i<mList.length;i++) {
          if(mcheckitems[i]==true) return(mList[i]);
        }
      }
      return "";
    } else return mChosen;
  }

  private Context context;


	
  public Dialog listselector(Context c, String title) {
    AlertDialog.Builder builder = new Builder(c);
    context=c;
    builder.setTitle(title);
    if(mList == null){
      return builder.create();
    }
    if(multichoice) {
      builder.setMultiChoiceItems(mList, mcheckitems, new DialogInterface.OnMultiChoiceClickListener(){
        @Override
        public void onClick(DialogInterface dialog, int which,boolean isChecked){
          mChosen= mList[which];
          //you can do stuff with the file here too
          Log.d(TAG,"Chosen: "+mChosen);
        }
      });
    } else {
      builder.setItems(mList, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which){
          mChosen= mList[which];
	  mSelected=which;
          //you can do stuff with the file here too
          Log.d(TAG,"Chosen: "+mChosen);
        }
      });
    } 
    builder.setOnCancelListener(new OnCancelListener() {
      @Override
      public void onCancel(DialogInterface arg0) {



      }
    });
    dialog=builder.create();

    dialog.setOnShowListener(new OnShowListener() {       
      @Override
      public void onShow(DialogInterface ldialog) {
        ListView lv = dialog.getListView(); //this is a ListView with your "buds" in it
        lv.setOnItemLongClickListener(new OnItemLongClickListener() {
          @Override
          public boolean onItemLongClick(AdapterView<?> parent, View view, int which, long id) {
            Log.d(TAG,"List Item #"+which+" was long clicked");
            // listdialog(mFileList[which]).show();
            //dialog.dismiss();
            return true;
          }
        });	
      }
    }); 
    return dialog;
  }
}
