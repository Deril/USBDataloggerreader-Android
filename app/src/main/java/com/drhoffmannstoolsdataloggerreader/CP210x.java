package com.drhoffmannstoolsdataloggerreader;

/* CP210x.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import android.util.Log;

/*   Was noch nicht geht: 
 *   Einstellung rollower oder nicht
 *   rollovercount anzeigen
 *   LCD on/OFF/30sek off
 *   LED Blikinterval --> off
 *   Fahrenheiteinstellung ???
 *   */



public class CP210x {
  private static final String TAG = "CP210X"; 
  private static String loggerstatus="DL-141 logger is not fully supported yet!";
  public static final int DEFAULT_BAUD_RATE = 9600;    
	
    
  public static final int SILABSER_IFC_ENABLE_REQUEST_CODE = 0x00;
  public static final int SILABSER_SET_BAUDDIV_REQUEST_CODE = 0x01;
  public static final int SILABSER_SET_LINE_CTL_REQUEST_CODE = 0x03;
  public static final int SILABSER_SET_MHS_REQUEST_CODE = 0x07;
  public static final int SILABSER_SET_BAUDRATE = 0x1E;

  public static final int UART_ENABLE = 0x0001;
  public static final int UART_DISABLE = 0x0000;

  public static final int BAUD_RATE_GEN_FREQ = 0x384000;

  public static final int MCR_DTR = 0x0001;
  public static final int MCR_RTS = 0x0002;
  public static final int MCR_ALL = 0x0003;
  
  public static final int CONTROL_WRITE_DTR = 0x0100;
  public static final int CONTROL_WRITE_RTS = 0x0200;

  private final static int SEK10=0x10;
  private final static int SEK20=0x20;
  private final static int SEK30=0x30;
  private final static int SEK00=0x40;
  private final static int NOWRAP=0x40;
  private final static int NOALM=0x20;
  private final static int NOAUTO=0x80;
  private final static int UNITSEK=0x1;
  private final static int UNITMIN=0x2;
  private final static int UNITHOUR=0x3;
  private final static int LCDOFF=0x10;  /* geraten ??*/
  private final static int LCD30sekOFF=0x10;  /* geraten ??*/
    
  /* Config Kommandotypen*/

  private final static int CMD_STD=0x8; // Standard parameter block
  private final static int CMD_REQ=0x68; // Request Transfer of standard parameter block
  private final static int CMD_BAT=0x79; // Take a battery level reading (factory use only)*

  public static byte[] build_conf(Logger logger) {
    ByteBuffer mes=ByteBuffer.allocate(7);
    mes.position(0);
    mes.order(ByteOrder.BIG_ENDIAN);
    int f=0;
    if((logger.config.led_conf&0x1f)==10) f+=SEK10;
    else if((logger.config.led_conf&0x1f)==20) f+=SEK20;
    else if((logger.config.led_conf&0x1f)==30) f+=SEK30;
    else f+=SEK00;
    mes.put(0,(byte)(CMD_STD+f)); 
    int interval=logger.config.set_interval;
    int unit=UNITSEK;
    int flags=0;
    if(interval>255*60) {
      interval=interval/3600;
      unit=UNITHOUR;
    } else if(interval>255) {
      interval=interval/60;
      unit=UNITMIN;
    }
    if(logger.config.start!=2) flags+=NOAUTO;
    if((logger.config.led_conf&0x80)!=0x80) flags+=NOALM;

    flags+=NOWRAP;  /* Kann man noch nicht einstellen.*/

    mes.put(1,(byte)(flags+unit)); 
    mes.put(2,(byte)interval); 
    mes.put(3,(byte)logger.config.templimit_high); 
    mes.put(4,(byte)logger.config.templimit_low); 
    mes.put(5,(byte)logger.config.humilimit_high); 
    mes.put(6,(byte)logger.config.humilimit_low); 
    return mes.array();
  }

  private static byte[] build_timeconf(Logger logger) {
    ByteBuffer mes=ByteBuffer.allocate(7);
    mes.position(0);
    mes.order(ByteOrder.BIG_ENDIAN);
    mes.put(0,(byte)0x58); 
    mes.put(1,(byte)rbcd(logger.config.time_year-2000));
    mes.put(2,(byte)rbcd(logger.config.time_mon));
    mes.put(3,(byte)rbcd(logger.config.time_mday));
    mes.put(4,(byte)rbcd(logger.config.time_hour));
    mes.put(5,(byte)rbcd(logger.config.time_min));
    mes.put(6,(byte)rbcd(logger.config.time_sec));
    return mes.array();
  }

  public static int chk_conf(LoggerConfig config, int loggertype, int memory_size) {
    String n=config.getname();
    if(n.length()==0) return Error.ERR_CNONAME;
    if(n.length()>16) return Error.ERR_CNAME;
    if(config.num_data_conf*4>memory_size || config.num_data_conf<0) return Error.ERR_NUMDATA;
    if(config.set_interval>918000 ||config.set_interval<=0) return Error.ERR_INTERVAL;
    return Error.OK;
  }

  private static byte[] readmessage(Logger logger, int len) {
    byte cbuf[]=new byte[0];
    byte obuf[];
    byte buf[]=new byte[logger.packet_size];
    int i=0;
    int r;
    while(i<len) {
      r=logger.receive(buf,logger.packet_size,5000);
      if(r<=0) break;
      obuf=cbuf;
      cbuf=new byte[i+r];
      if(i>0) System.arraycopy(obuf, 0, cbuf, 0, i);
      System.arraycopy(buf, 0, cbuf, i, r);
      i+=r;
      try { Thread.sleep(150); } 
      catch (InterruptedException e1) {logger.log2comproto("sleep was interrupted: "+e1.getMessage(),1);}
    }
    logger.log2comproto("receive<--"+Logger.bytearray2string(cbuf), 2);
    return(cbuf);
  }

  private static void sendmessage(Logger logger,byte[] message) {
    logger.log2comproto("send	-->"+Logger.bytearray2string(message), 2);
    logger.send(message);
    try { Thread.sleep(50); } 
    catch (InterruptedException e1) {logger.log2comproto("sleep was interrupted: "+e1.getMessage(),1);}
  }

  public static byte[] read_configblock(Logger logger) {
    Log.d(TAG,"readconfigblock...");
    byte cbuf[]=new byte[2];
    byte[] message = new byte[1];
    message[0]=0;
    sendmessage(logger, message);
    cbuf=readmessage(logger, 3);
    /*Versionsinformationen ???*/
    int s=0,i=0;
    while(i<cbuf.length) {
      if(cbuf[i]!=0) s=(s<<8) | (cbuf[i]&255);
      i++;
    }
    logger.config.sensor_type=s;
		
    message=new byte[7];
    message[0]='h';
    message[1]='h';
    message[2]='h';
    message[3]='h';
    message[4]='h';
    message[5]='h';
    message[6]='h';
    sendmessage(logger, message);
    cbuf=readmessage(logger, 21);
    /*Nullbytes am Anfang entfernen:*/
    if(cbuf.length>0 && cbuf[0]==0) {
      cbuf=Arrays.copyOfRange(cbuf, 1,cbuf.length); 
    }
    return cbuf;
  }

  private static byte[] read_membank(Logger logger,int n) {
    Log.d(TAG,"readmembank "+n);
    byte cbuf[];  /*1/100 tel des Gesamtspeichers*/
    byte[] message = new byte[7];
    message=new byte[7];
    message[0]='h';
    message[1]='h';
    message[2]='h';
    message[3]='h';
    message[4]='h';
    message[5]='h';
    message[6]=(byte)n;
    sendmessage(logger, message);
    cbuf=readmessage(logger,164*4);
    return cbuf;
  }
	
  private static int bcd(int a)  {return((a&0xf)+10*(a>>4));}
  private static int rbcd(int a) {return(((a/10)<<4)+(a%10));}

  //Experimentell
  public static int read_battery_value(Logger logger) {
    Log.d(TAG,"read battery value...");
    byte cbuf[]=new byte[2];
    byte[] message = new byte[1];
    message[0]=CMD_BAT;
    sendmessage(logger, message);
    cbuf=readmessage(logger, 1);
    if(cbuf==null) return Error.ERR;
    return (cbuf[0]&0xff);  /* The response is 1 Byte: 70 -- 100% */
  }

  public static int get_config(Logger logger) {
    byte cbuf[]=read_configblock(logger);
    if(cbuf==null) return Error.ERR;
    else logger.updateMessage("Config OK. ",0);
    int ret=Error.OK;
    if(cbuf.length!=21 || cbuf[0]!=(byte)0xdd || cbuf[20]!=(byte)0xaa) {
      return Error.ERR_BAD;
    } else { 
      loggerstatus="Config OK: ";
      LoggerConfig config=logger.config;
      config.mglobal_message="";
      config.flag_bits=0;
      config.time_delay=0;
      config.setname(logger.Serial_id);

      ByteBuffer buffer = ByteBuffer.allocate(cbuf.length);
      buffer.position(0);
      buffer.put(cbuf);
      buffer.position(0);
      buffer.order(ByteOrder.BIG_ENDIAN);
      config.config_begin=0xdd;
      config.num_data_conf=16350;
      int interval=(buffer.get(6)&0xff);
      if((buffer.get(5)&0xff)==0x20) interval*=3600;
      else if((buffer.get(5)&0xff)==0x30) interval*=60;
      else if((buffer.get(5)&0xff)==0x60) interval*=1;
			
      config.got_interval=interval;
      config.set_interval=config.got_interval;
      config.num_data_rec=(buffer.getShort(1) & 0xffff)/4;
      config.rollover_count=(buffer.getShort(3) & 0xffff);
      config.templimit_high=(buffer.get(14));
      config.templimit_low=(buffer.get(15));
      config.humilimit_high=(buffer.get(16));
      config.humilimit_low=(buffer.get(17));
      config.temp_unit=((buffer.get(18)==0x24)?Logger.UNIT_F:Logger.UNIT_C);
      // buffer.get(19): 0x78 = ++, 0x76 = +-, 0x77 =--
      config.time_hour=(byte) bcd(buffer.get(11)&0xff);
      config.time_min=(byte) bcd(buffer.get(12)&0xff);
      config.time_sec=(byte) bcd(buffer.get(13)&0xff);
      config.time_year=2000+bcd(buffer.get(8)&0xff);
      config.time_mon=(byte) bcd(buffer.get(9)&0xff);
      config.time_mday=(byte) bcd(buffer.get(10)&0xff);
      // config.led_conf=10; unveraendert.
      config.config_end=(buffer.get(20)&0xff);
      // config.start=2; unveraendert.
    }
    logger.isreadconfig=true;
    return ret;
  }

  public static int write_configblock(byte[] cbuf,Logger logger) {
    if(!logger.isconnected) {
      Log.d(TAG, "connection problem ");
      return(Error.ERR);
    }
    int ret=Error.OK;
    byte rbuf[];
    Log.d(TAG,"writetimeconfigblock...");
    sendmessage(logger,build_timeconf(logger));
    rbuf=readmessage(logger, 3);
    if(rbuf.length==0) ret=Error.ERR;
    else {
      int i=0,flag=0;
      while(i<rbuf.length) {
	if(rbuf[i]==(byte)0xee) flag=1;
	i++;
      }
      if(flag==0) ret=Error.ERR;
    }
    Log.d(TAG,"writeconfigblock...");
    sendmessage(logger,cbuf);
    cbuf=readmessage(logger, 3);
    if(cbuf.length==0) return(Error.ERR);
    int i=0,flag=0;
    while(i<cbuf.length) {
      if(cbuf[i]==30) flag=1;
      i++;
    }
    if(flag==0) return(Error.ERR);
    return(ret);
  }

  public static int get_data(Logger logger) {
    int q=0,count=0,pointer=0;
    byte[] buf;
    byte[] mem=new byte[logger.memory_size];
    final LoggerData data=logger.data;
    data.quality=0;
    int j;
    int i=logger.config.num_data_rec*4;
    logger.updateProgress(0);
    logger.updateStatus(0);
    while(i>0) {
      buf=read_membank(logger,count);
      if(buf.length<=0) break;
      System.arraycopy(buf, 0, mem,pointer , buf.length);
      pointer+=buf.length;
      i-=buf.length;
      count++;
      logger.updateProgress((int)(pointer*100.0/logger.config.num_data_rec/4));
    }
    for(j=0;j<pointer/4;j++) {
      data.add((short) ((short)mem[j*4+1]&0xff | ((short)mem[j*4]&0xff)<<8),
    	       (short) ((short)mem[j*4+3]&0xff | ((short)mem[j*4+2]&0xff)<<8));
    }
    logger.updateProgress(100);
    for(i=0;i<data.anzdata;i++) {
      if(data.get_temp(i)>200 || data.get_temp(i)<-100 || 
    	 data.get_rh(i)>104 || data.get_rh(i)<-1) q++;
    }
    return(q);
  }

  public static String get_status(Logger logger) {
    String ret=loggerstatus; 
    if(logger.isreadconfig) {
      ret=logger.typestring()+": typ="+logger.loggertype;
      if(logger.config.config_begin==0) ret=ret+": normal,";
      else ret=ret+": manual/delayed started, ("+logger.config.config_begin+"),";
      if(logger.config.num_data_rec==0) ret=ret+" NO DATA";
      else ret=ret+" DATA available ("+logger.config.num_data_rec+")";
    } else ret="Config has not yet been read";
    return ret+".";
  }
}
