package com.drhoffmannstoolsdataloggerreader;

/* LoggerData.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import android.os.Environment;
import android.util.Log;

public class LoggerData {
  private static final String TAG = "LoggerData";
  public int anzdata=0;
  public double interval;
  /* The maximum number of data is given by the loggers
     memory size. This is for the moment and for all supported loggers
     64kBytes. */
  public final static int MAX_DATAPOINTS=65536;
  public short temp[]=new short[MAX_DATAPOINTS/2]; /*Given 2 Bytes per sample */
  public short rh[]=new short[MAX_DATAPOINTS/3]; /*Given 3 Bytes, sometimes 4 per sample */
  public short p[]=new short[MAX_DATAPOINTS/6]; /*Given 6 Bytes per sample */
  public ArrayList<Lueftungsevent> eventliste;

  public int quality=0; 	    // number of strange values
  public boolean issaved=false;
  /*Calibration zum Zeitpunkt, als die Daten gelesen wurden.*/
  public float calibration_Avalue=(float)0.1; 
  public float calibration_Bvalue=(float)0.0; 
  public float calibration_A2value=(float)0.1; 
  public float calibration_B2value=(float)0.0;

  public void clear() {
    anzdata=0;
    quality=0;
    issaved=false;
    eventliste = null;
    set_calibration((float)0.1,(float)0,(float)0.1,(float)0);
  }
  public void set_interval(double dt) { interval=dt; }
  public void set_calibration(float a,float b, float a2,float b2) {
    calibration_Avalue=a;
    calibration_Bvalue=b; 
    calibration_A2value=a2; 
    calibration_B2value=b2;
  }

  public void add(short t) { if(anzdata<temp.length) temp[anzdata++]=t; }
  public void add(short t, short r) {
    if(anzdata<rh.length)    rh[anzdata]=r;
    if(anzdata<temp.length)  temp[anzdata++]=t;
  }
  public void add(short t, short r,short pr) {
    if(anzdata<rh.length)     rh[anzdata]=r;
    if(anzdata<p.length)       p[anzdata]=pr;
    if(anzdata<temp.length) temp[anzdata++]=t;
  }

  public float get_temp(int i) {
    if(i<0 || i>=anzdata) return(-1);
    return temp[i]*calibration_Avalue+calibration_Bvalue;
  }
  public float get_temp(short t) {
    return t*calibration_Avalue+calibration_Bvalue;
  }
  public float get_rh(int i) {
    if(i<0 || i>=anzdata) return(-1);
    return rh[i]*calibration_A2value+calibration_B2value;
  }
  public float get_rh(short r) {
    return r*calibration_A2value+calibration_B2value;
  }

	private int probe(int i) {
		int j=i;
		while((interval*(j-i)<5*60 || j-i<3) && j<anzdata-1) j++;
		return j;
	}

	public int calc_events() {
		eventliste = new ArrayList<Lueftungsevent>();
		int j,kk=0;
		Lueftungsevent ev=new Lueftungsevent();
		ev.idx=-1;
		for(int i=0;i<anzdata-3;i++) {
			j=probe(i);
			double w1=Physics.water(get_temp(i), get_rh(i));
			double w2=Physics.water(get_temp(j), get_rh(j));
			if(j-i>2 && w2-w1<-0.02*w2 && get_temp(j)-get_temp(i)<-0.2) {
				if(kk==0 || i>kk) {
					if(ev.idx!=-1) {
						eventliste.add(ev);
						ev=new Lueftungsevent();
					}
					ev.idx=i;
					ev.idx2=j;
					ev.dt=get_temp(j)-get_temp(i);
					ev.dr=w2-w2;
				} else {
					w1=Physics.water(get_temp(ev.idx), get_rh(ev.idx));
					ev.dr=Math.min(ev.dr,w2-w1);
					ev.dt=Math.min(ev.dt,get_temp(j)-get_temp(ev.idx));
					ev.idx2=j;
				}
				kk=j;
			}
		}
		if(ev.idx!=-1) eventliste.add(ev);
		return eventliste.size();
	}
	/*Save data to file....*/
	public static int save(Logger logger, String filename,String endung,boolean append,
			String titlestring) {
		final LoggerConfig config=logger.config;
		final LoggerData data=logger.data;
		FileOutputStream fos=null;
		OutputStreamWriter osw=null;
		File file=null;

		Calendar cal = Calendar.getInstance();
		cal.set(config.time_year, (int)(config.time_mon&0xff)-1, (int)(config.time_mday&0xff), (int)(config.time_hour&0xff), (int)(config.time_min&0xff), (int)(config.time_sec&0xff));
		long ss=cal.getTimeInMillis()/1000;

		String infostring="["+String.format(Locale.US,"%04d-%02d-%02d %02d:%02d:%02d",config.time_year,(int)(config.time_mon&0xff),
				(int)(config.time_mday&0xff),(int)(config.time_hour&0xff),(int)(config.time_min&0xff),
				(int)(config.time_sec&0xff))+"] "+config.num_data_rec+" points @ "+config.got_interval+" sec.";
		String loggerinfostring=logger.Manufacturer+"-"+logger.Product+"/"+logger.loggertype+" #"+logger.Serial_id+" blocktype="+config.block_type
				+" Session: "+config.getname();
		String calibrationinfostring="A="+data.calibration_Avalue+" B="+data.calibration_Bvalue+" "+
				"A2="+data.calibration_A2value+" B2="+data.calibration_B2value+" "+
				"M="+config.calibration_Mvalue+" C="+config.calibration_Cvalue;
		String separator=" ";
		if(endung.equalsIgnoreCase("csv")) separator=", ";
		String legende="UNIX time [s]"+separator;
		if(logger.loggertype==Logger.LTYP_VOLT) 
			legende=legende+"Voltage ["+config.getunit()+"]"
					+separator+separator+separator;
		else if(logger.loggertype==Logger.LTYP_CURR) 
			legende=legende+"Current ["+config.getunit()+"]"
					+separator+separator+separator;
		else {
			legende=legende+"Temperature ["+((config.temp_unit==Logger.UNIT_F)?"°F":"°C")+"]"+
					separator;
			if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) {
				legende=legende+"Humidity [%]"+separator;
				if(logger.loggertype==Logger.LTYP_THP) legende=legende+"Pressure [hPa]"+separator;
				else legende=legende+separator;            			
			} else legende=legende+separator+separator;
		}
		legende=legende+"Date"+separator+"Time";
		/*Weil gewünscht hier noch Taupunkt und Wassergehalt:*/
		if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) {
			legende=legende+separator+"Dew Point ["+((config.temp_unit==Logger.UNIT_F)?"°F":"°C")+"]"+separator+"Water content [g/m³]";
		}

		String settingsinfo="";
		String statistics="";
		String eventstat="";
		if(logger.loggertype==Logger.LTYP_THP) 
			settingsinfo+=String.format(Locale.US,"# Pressure limits: [%.2f:%.2f] hPa\n",config.preslimit_low,config.preslimit_high);
		if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH) 
			settingsinfo+=String.format(Locale.US,"# Humidity limits: [%.1f:%.1f] %%\n",config.humilimit_low,config.humilimit_high);
		if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_TEMP) 
			settingsinfo+=String.format(Locale.US,"# Temperature limits: [%.1f:%.1f] %s\n",config.templimit_low,config.templimit_high,
					((config.temp_unit==Logger.UNIT_F)?"°F":"°C"));
		if(logger.loggertype==Logger.LTYP_VOLT || logger.loggertype==Logger.LTYP_CURR)
			settingsinfo+=String.format(Locale.US,"# Limits: [%f:%f] %s\n",config.templimit_low,config.templimit_high,config.getunit());

		if(config.start==1) settingsinfo+=String.format(Locale.US,"# Start delay=%d sec\n",config.time_delay);

		/* Datenstatistik erstellen */

		int duration=data.anzdata*config.got_interval;
		statistics=statistics+"# Time range: ["+ss+":"+(ss+duration)+"] "+Physics.duration_dhms(duration)+"\n";

		float tmin=9999,tmax=-9999,t;
		for(int i=0;i<data.anzdata;i++) {
			t=data.get_temp(i);
			tmin=Math.min(tmin,t);
			tmax=Math.max(tmax,t);
		}
		if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_TEMP) 
			statistics=statistics+String.format(Locale.US,"# Temperature range: [%.1f:%.1f] %s\n",
					tmin,tmax,((config.temp_unit==Logger.UNIT_F)?"°F":"°C"));
		else if(logger.loggertype==Logger.LTYP_VOLT || logger.loggertype==Logger.LTYP_CURR) 
			statistics=statistics+String.format(Locale.US,"# Range: [%f:%f] %s\n",tmin,tmax,config.getunit());;
			if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH) {
				float rhmin=9999,rhmax=-9999,r;
				for(int i=0;i<data.anzdata;i++) {
					r=data.get_rh(i);
					rhmin=Math.min(rhmin,r);
					rhmax=Math.max(rhmax,r);
				} 
				statistics=statistics+String.format(Locale.US,"# Humidity range:    [%.1f:%.1f] %%\n",
						rhmin,rhmax);	
				/* Events zusammenfassen   */

				if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH) {
					if(data.eventliste!=null && data.eventliste.size()>0) {
						eventstat+="# Detected "+data.eventliste.size()+" Events:\n";
						Lueftungsevent a;
						for(int i=0;i<data.eventliste.size();i++) {
							a=data.eventliste.get(i);
							eventstat+="# "+i+": "+Physics.timestamp2datetime_short(ss+config.got_interval*a.idx)+
									" for "+Physics.gerundet(config.got_interval*(a.idx2-a.idx)/60,1)+" minutes.\n";
						}
						eventstat+="# \n";
					}
				}

			}
			if(logger.loggertype==Logger.LTYP_THP) {
				float pmin=9999,pmax=-9999;
				for(int i=0;i<data.anzdata;i++) {
					pmin=Math.min(pmin,data.p[i]);
					pmax=Math.max(pmax,data.p[i]);
				} 
				statistics=statistics+String.format(Locale.US,"# Pressure range:    [%.2f:%.2f] hPa\n",
						Voltcraft.raw2p((short)pmin),Voltcraft.raw2p((short)pmax));

			}
			if(eventstat.length()>0) statistics+=eventstat;










			try {
				File dirdata=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
				dirdata.mkdirs();

				file=new File(dirdata,filename);
				fos=new FileOutputStream(file,append);
				osw=new OutputStreamWriter(fos);
				if(endung.equalsIgnoreCase("xml")) {
					osw.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"+
							"<metadata>\n");
					osw.write("  <link href=\"https://play.google.com/store/apps/details?id=com.drhoffmannstoolsdataloggerreader\">\n");
					osw.write("    <text>"+titlestring+"</text>\n");
					osw.write("  </link>\n");
					osw.write("  <time>"+String.format(Locale.US,"%04d-%02d-%02dT%02d:%02d:%02dZ",config.time_year,(int)(config.time_mon&0xff),
							(int)(config.time_mday&0xff),(int)(config.time_hour&0xff),(int)(config.time_min&0xff),
							(int)(config.time_sec&0xff))+"</time>\n");
					osw.write("  <datalogger>\n");
					osw.write("    <device>"+loggerinfostring+"</device>\n");
					osw.write("    <Sampletime>"+config.got_interval+"</Sampletime>\n");
					osw.write("    <serial>"+logger.Serial_id+"</serial>\n");
					osw.write("  <calibration>"+calibrationinfostring+"</calibration>\n");
					osw.write("  </datalogger>\n");
					osw.write("  <filename>"+filename+"</filename>\n");
					osw.write("</metadata>\n");
					osw.write("<log>\n");
					osw.write("  <name>"+config.getname()+"</name>\n");
					osw.write("  <subset>"+infostring+"</subset>\n");
					osw.write("  <legend>"+legende+"</legend>\n");

				} else if(endung.equalsIgnoreCase("csv")) {
					osw.write(legende+"\n"); /*erste Zeile ist legende*/
				} else {
					osw.write("# "+titlestring+"\n");
					osw.write("# loggertype:  "+loggerinfostring+"\n");
					osw.write("# Rawinput="+config.rawinputreading+" flagbits="+config.flag_bits+"\n");
					osw.write("# Calibration: "+calibrationinfostring+"\n");
					osw.write("# fileformat="+endung+" append="+append+"\n");
					osw.write(settingsinfo);
					osw.write("# Statistics:\n"+statistics);
					// Anzahl Schwellenüberschreitungen
					// ANzahl Lüftungsevents, Taupunkt Min/Max, Wassergehalt min/max
					// Evtl Histogramm
					osw.write("# "+infostring+"\n");
					osw.write("# "+legende+"\n#\n");
				}	
				int i;

				String dateS,timeS;
				for(i=0;i<data.anzdata;i++) {
					cal = Calendar.getInstance();
					cal.setTimeInMillis(ss*1000);
					dateS=String.format(Locale.US,"%02d.%02d.%04d", 
							cal.getTime().getDate(),
							(cal.getTime().getMonth()+1),
							(cal.getTime().getYear()+1900));
					timeS=String.format(Locale.US,"%02d:%02d:%02d",
							cal.getTime().getHours(),
							cal.getTime().getMinutes(),
							cal.getTime().getSeconds());
					if(endung.equalsIgnoreCase("xml")) {
						osw.write("  <sample timestamp=\""+ss+"\">\n");
						osw.write("    <channel num=\"1\">\n");
						osw.write("      <rawdata>"+data.temp[i]+"</rawdata>\n");
						osw.write("      <value>"+data.get_temp(i)+"</value>\n");
						if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_TEMP) {
							osw.write("      <temperature>"+data.get_temp(i)+"</temperature>\n");
							osw.write("      <unit>"+((config.temp_unit==Logger.UNIT_F)?"°F":"°C")+"</unit>\n");
						} else osw.write("      <unit>"+config.getunit()+"</unit>\n");
						osw.write("    </channel>\n");
						if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) {
							osw.write("    <channel num=\"2\">\n");
							osw.write("      <rawdata>"+data.rh[i]+"</rawdata>\n");
							osw.write("      <value>"+data.get_rh(i)+"</value>\n");
							osw.write("      <humidity>"+data.get_rh(i)+"</humidity>\n");
							osw.write("      <unit>%</unit>\n");
							osw.write("    </channel>\n");
							osw.write("    <dewpoint>"+Physics.taupunkt(data.get_temp(i),data.get_rh(i))+"</dewpoint>\n");
							osw.write("    <water>"+Physics.water(data.get_temp(i),data.get_rh(i))+"</water>\n");
						}
						if(logger.loggertype==Logger.LTYP_THP) {
							osw.write("    <channel num=\"3\">\n");
							osw.write("      <rawdata>"+data.p[i]+"</rawdata>\n");
							osw.write("      <value>"+Voltcraft.raw2p(data.p[i])+"</value>\n");
							osw.write("      <pressure>"+Voltcraft.raw2p(data.p[i])+"</pressure>\n");
							osw.write("      <unit>hPa</unit>\n");
							osw.write("    </channel>\n");
						} 

						osw.write("    <time>"+dateS+"T"+timeS+"Z</time>\n");
						osw.write("  </sample>\n");
					} else {
						osw.write(""+ss+separator+Physics.gerundet(data.get_temp(i),5)+separator);
						if(logger.loggertype==Logger.LTYP_THP) {
							osw.write(Physics.gerundet(data.get_rh(i),2)+separator+
									Voltcraft.raw2p(data.p[i])+separator);
						} else if(logger.loggertype==Logger.LTYP_TH) {
							osw.write(Physics.gerundet(data.get_rh(i),2)+separator+"0"+separator);
						} else osw.write("0"+separator+"0"+separator);
						osw.write(dateS+separator+timeS);
						if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) {
							if((config.temp_unit==Logger.UNIT_F)) osw.write(separator+
									Physics.gerundet(Physics.celsius2fahr(Physics.taupunkt((float)Physics.fahr2celsius(data.get_temp(i)),data.get_rh(i))),2)
									+separator+
									Physics.gerundet(Physics.water((float)Physics.fahr2celsius(data.get_temp(i)),data.get_rh(i)),2)
									);
							else osw.write(separator+Physics.gerundet(Physics.taupunkt(data.get_temp(i),data.get_rh(i)),2)+
									separator+Physics.gerundet(Physics.water(data.get_temp(i),data.get_rh(i)),2));
						}
						osw.write("\n");
					}
					ss+=config.got_interval;
//					mProgressStatus=i*100/data.anzdata;
//					h.post(new Runnable() {
//						public void run() {
//							progress1.setProgress(mProgressStatus);
//						}
//					});
				}
				if(endung.equalsIgnoreCase("xml")) {
					osw.write("</log>\n");
				} else if(endung.equalsIgnoreCase("csv")) {
					osw.write("# "+titlestring+"\n");
					osw.write("# loggertype:  "+loggerinfostring+"\n");
					osw.write("# Calibration: "+calibrationinfostring+"\n");
					osw.write("# Rawinput="+config.rawinputreading+" flagbits="+config.flag_bits+"\n");
					osw.write(settingsinfo);
					osw.write("# "+infostring+"\n");
				}

				data.issaved=true;

			} catch (Throwable thr) {
				// FilenotFound oder IOException
				Log.e(TAG,"open/save. ",thr);
    } finally {
      if(osw!=null) {try {osw.close();} catch (IOException e) {Log.e(TAG,"osw.close ",e);}}
      if(fos!=null) {try {fos.close();} catch (IOException e) {Log.e(TAG,"fos.close ",e);}}
      if(data.issaved) return(Error.OK);
    }
    return(Error.ERR);
  }
}
