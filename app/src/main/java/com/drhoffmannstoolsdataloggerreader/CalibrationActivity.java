package com.drhoffmannstoolsdataloggerreader;

/* CalibrationActivity.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

public class CalibrationActivity extends Activity {

	private  CheckBox pcorr1,histograms,grid,event,label;
	private  CheckBox dewpoint1,lowpassp,fixscale1,fixscale2,fixscale3;
	private  EditText fileselect;
	private  EditText latency,lowpassporder;
	private  EditText factor;
	private  EditText minscale1, maxscale1, stepscale1;
	private  EditText minscale2, maxscale2, stepscale2;
	private  EditText minscale3, maxscale3, stepscale3;
	private  RadioButton bt1,bt2,bt3,bt4,bt5;
	private  SharedPreferences settings;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.calibration);
		ActionBar actionBar = getActionBar();
		//  actionBar.setHomeButtonEnabled(true);
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setTitle(R.string.title_PCS);
		}

		pcorr1=(CheckBox) findViewById(R.id.checkBox_pcorr);
		lowpassp=(CheckBox) findViewById(R.id.lowpassp);
		dewpoint1=(CheckBox) findViewById(R.id.checkBox_dewpoint);
		histograms=(CheckBox) findViewById(R.id.checkBox_histogram);
		grid=(CheckBox) findViewById(R.id.checkBox_grid);
		event=(CheckBox) findViewById(R.id.checkBox_event);
		label=(CheckBox) findViewById(R.id.checkBox_label);
		
		fixscale1=(CheckBox) findViewById(R.id.fixscale1);
		fixscale2=(CheckBox) findViewById(R.id.fixscale2);
		fixscale3=(CheckBox) findViewById(R.id.fixscale3);
		bt1=(RadioButton)findViewById(R.id.bt1);
		bt2=(RadioButton)findViewById(R.id.bt2);
		bt3=(RadioButton)findViewById(R.id.bt3);
		bt4=(RadioButton)findViewById(R.id.bt4);
		bt5=(RadioButton)findViewById(R.id.bt5);
		fileselect=(EditText) findViewById(R.id.fileselect);
		latency=(EditText) findViewById(R.id.latency);
		factor=(EditText) findViewById(R.id.factor);
		lowpassporder=(EditText) findViewById(R.id.lowpassporder);
		minscale1=(EditText) findViewById(R.id.minscale1);
		minscale2=(EditText) findViewById(R.id.minscale2);
		minscale3=(EditText) findViewById(R.id.minscale3);
		maxscale1=(EditText) findViewById(R.id.maxscale1);
		maxscale2=(EditText) findViewById(R.id.maxscale2);
		maxscale3=(EditText) findViewById(R.id.maxscale3);
		stepscale1=(EditText) findViewById(R.id.stepscale1);
		stepscale2=(EditText) findViewById(R.id.stepscale2);
		stepscale3=(EditText) findViewById(R.id.stepscale3);
		settings =PreferenceManager.getDefaultSharedPreferences(getBaseContext());
	}
	@Override
	public void onResume() {
		super.onResume();
		pcorr1.setChecked(settings.getBoolean("pcorr", false));
		dewpoint1.setChecked(settings.getBoolean("dewpoint", false));
		histograms.setChecked(settings.getBoolean("histograms", false));
		grid.setChecked(settings.getBoolean("grid", true));
		event.setChecked(settings.getBoolean("event", false));
		label.setChecked(settings.getBoolean("label", false));
	
		fixscale1.setChecked(settings.getBoolean("fixscale1", false));
		fixscale2.setChecked(settings.getBoolean("fixscale2", false));
		fixscale3.setChecked(settings.getBoolean("fixscale3", false));
		bt1.setChecked(settings.getInt("bluecurvetype", 1)==1);
		bt2.setChecked(settings.getInt("bluecurvetype", 1)==2);
		bt3.setChecked(settings.getInt("bluecurvetype", 1)==3);
		bt4.setChecked(settings.getInt("bluecurvetype", 1)==4);
		bt5.setChecked(settings.getInt("bluecurvetype", 1)==5);
		lowpassp.setChecked(settings.getBoolean("lowpassp", false));
		fileselect.setText(""+settings.getInt("fileselect", 8));
		latency.setText(""+settings.getFloat("pcorr_latenz", (float)7*60)/60);
		factor.setText(""+settings.getFloat("pcorr_fakt", (float)0.4));
		lowpassporder.setText(""+settings.getInt("lowpassporder", 1));
		minscale1.setText(""+settings.getFloat("minscale1", (float)0.0));
		minscale2.setText(""+settings.getFloat("minscale2", (float)0.0));
		minscale3.setText(""+settings.getFloat("minscale3", (float)700.0));
		maxscale1.setText(""+settings.getFloat("maxscale1", (float)100.0));
		maxscale2.setText(""+settings.getFloat("maxscale2", (float)100.0));
		maxscale3.setText(""+settings.getFloat("maxscale3", (float)1100.0));
		stepscale1.setText(""+settings.getFloat("stepscale1", (float)1.0));
		stepscale2.setText(""+settings.getFloat("stepscale2", (float)1.0));
		stepscale3.setText(""+settings.getFloat("stepscale3", (float)10.0));
	}
	@Override
	protected void onPause(){
		int bluecurvetype=0;
		super.onPause();

		// We need an Editor object to make preference changes.
		// All objects are from android.context.Context
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean("pcorr", pcorr1.isChecked());
		editor.putBoolean("dewpoint", dewpoint1.isChecked());
		editor.putBoolean("histograms", histograms.isChecked());
		editor.putBoolean("grid", grid.isChecked());
		editor.putBoolean("event", event.isChecked());
		editor.putBoolean("label", label.isChecked());
		
		editor.putBoolean("lowpassp", lowpassp.isChecked());		
		editor.putBoolean("fixscale1", fixscale1.isChecked());
		editor.putBoolean("fixscale2", fixscale2.isChecked());
		editor.putBoolean("fixscale3", fixscale3.isChecked());

		if(bt1.isChecked()) bluecurvetype=1;
		else if(bt2.isChecked()) bluecurvetype=2;
		else if(bt3.isChecked()) bluecurvetype=3;
		else if(bt4.isChecked()) bluecurvetype=4;
		else if(bt5.isChecked()) bluecurvetype=5;

		editor.putInt("bluecurvetype",bluecurvetype );
		editor.putInt("fileselect", (int)Double.parseDouble(fileselect.getText().toString()));
		editor.putInt("lowpassporder", (int)Double.parseDouble(lowpassporder.getText().toString()));

		editor.putFloat("pcorr_fakt",(float)Double.parseDouble(factor.getText().toString()));
		editor.putFloat("pcorr_latenz",(float)Double.parseDouble(latency.getText().toString())*60);
		
		editor.putFloat("minscale1",(float)Double.parseDouble(minscale1.getText().toString()));
		editor.putFloat("maxscale1",(float)Double.parseDouble(maxscale1.getText().toString()));
		editor.putFloat("stepscale1",(float)Double.parseDouble(stepscale1.getText().toString()));

		editor.putFloat("minscale2",(float)Double.parseDouble(minscale2.getText().toString()));
		editor.putFloat("maxscale2",(float)Double.parseDouble(maxscale2.getText().toString()));
		editor.putFloat("stepscale2",(float)Double.parseDouble(stepscale2.getText().toString()));

		editor.putFloat("minscale3",(float)Double.parseDouble(minscale3.getText().toString()));
		editor.putFloat("maxscale3",(float)Double.parseDouble(maxscale3.getText().toString()));
		editor.putFloat("stepscale3",(float)Double.parseDouble(stepscale3.getText().toString()));
		// Commit the edits!
		editor.apply();
	}
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId())  {
		case android.R.id.home:
			// This is called when the Home (Up) button is pressed
			// in the Action Bar.
			finish();
			return true;
		default: 
			return super.onOptionsItemSelected(item);
		}
	}
}
