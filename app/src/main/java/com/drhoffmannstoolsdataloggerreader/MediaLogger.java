package com.drhoffmannstoolsdataloggerreader;

/* MediaLogger.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * =============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import android.widget.Toast;
import android.os.Environment;
import android.os.Build;

import java.io.BufferedReader;
import java.io.StringReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;
import android.util.Xml;
import android.util.Base64;


public class MediaLogger {
  private static final String TAG = "MediaLogger"; 
  private static String loggerstatus="DL-2xx logger is not fully supported yet!";

  /* Vaiablen für Lokal Konfiguration */

  public String Location=null;
  public String Owner=null;
    
  /* Capabilities */
  
  public final static int capabilities=Logger.CAPA_STARTBUTTON|
                                       Logger.CAPA_STOPBUTTON|
                                       Logger.CAPA_STOPTIME|
                                       Logger.CAPA_STARTTIME|
                                       Logger.CAPA_DELAY|
                                       Logger.CAPA_PAUSE|
                                       Logger.CAPA_LIMITS|
                                       Logger.CAPA_LED|Logger.CAPA_ROLLOVER|
				       Logger.CAPA_LCD;

  /* Build the binary data from configuration */


  public static byte[] build_conf(Logger logger) {
    ByteBuffer mes=ByteBuffer.allocate(512);
    mes.position(0);
    mes.order(ByteOrder.LITTLE_ENDIAN);
    mes.put(0,(byte)0x3f); /* Field Marker */
    mes.put(1,(byte)0x2e);
    mes.put(2,(byte)0x30);
    String n=logger.Product;
    mes.position(3);
    mes.put(n.getBytes());
    mes.putChar(3+n.length(),(char) 0);

    mes.put(0x40,(byte)0x3f); /* Field Marker */
    mes.put(0x41,(byte)0x36);
    mes.put(0x42,(byte)0x11);

    mes.put(0x43,(byte)0x21); /* ??? */

    if(logger.config.set_interval<0) logger.config.set_interval=60;
    mes.putInt(0x4b,logger.config.set_interval);
    
    /* LED-Blinkintervall (5, 10, 15, 20, 25, 30 sec) */

    mes.put(0x4f,(byte)(logger.config.led_conf & 0x1f));
 
    /* Bedingungen für Aufzeichnungsstart: 
      00=umgehend bis Speicher voll ist, 
      01= Start bei Tastendruck, 
      02=Start zur Startzeit, 
      03=Start/Stopzeit, 
      04=Fortlaufende Protokollierung
     */
     if(logger.config.startcondition==Logger.START_INSTANT) {
       if((logger.config.stopcondition&Logger.STOP_MEMFULL)!=0) {
         mes.put(0x50,(byte)0);
       } else {
         mes.put(0x50,(byte)4);
       }
     } 
     else if(logger.config.startcondition==Logger.START_BUTTON) {
         mes.put(0x50,(byte)1);
     }
     else if(logger.config.startcondition==Logger.START_TIME) {
       if((logger.config.stopcondition&Logger.STOP_TIME)!=0) {
         mes.put(0x50,(byte)3);
       } else {
         mes.put(0x50,(byte)2);
       }
     }
     else mes.put(0x50,logger.config.start); 

     /* ALARM typ flags 
       1 = Alarm on
       2 = temperature limits on
       4 = Humidity limits on
       8 = Pressure limits on 
      */
    byte flags=0;
    if((logger.config.alarmtyp&Logger.ALARM_ON)!=0) flags|=1;
    if((logger.config.alarmtyp&Logger.ALARM_T)!=0) flags|=2;
    if((logger.config.alarmtyp&Logger.ALARM_H)!=0) flags|=4;
    if((logger.config.alarmtyp&Logger.ALARM_P)!=0) flags|=8;
    mes.put(0x51,flags);


    if((logger.config.alarmtyp&Logger.ALARM_T)!=0) {
      if(logger.config.templimit_low>logger.config.templimit_high) {
        float t=logger.config.templimit_low;
	logger.config.templimit_low=logger.config.templimit_high;
	logger.config.templimit_high=t;
      }
      if(logger.config.templimit_low<-40)logger.config.templimit_low=-40;
      if(logger.config.templimit_high<-40)logger.config.templimit_high=-40;
      if(logger.config.templimit_low>60)logger.config.templimit_low=60;
      if(logger.config.templimit_high>60)logger.config.templimit_high=60;
      
      mes.putShort(0x52,(short)(logger.config.templimit_low*100));
      mes.putShort(0x54,(short)(logger.config.templimit_high*100));
    } else {
      mes.putShort(0x52,(short)0);
      mes.putShort(0x54,(short)0);
    }
    if((logger.config.alarmtyp&Logger.ALARM_H)!=0) {
      if(logger.config.humilimit_low>logger.config.humilimit_high) {
        float t=logger.config.humilimit_low;
	logger.config.humilimit_low=logger.config.humilimit_high;
	logger.config.humilimit_high=t;
      }
      if(logger.config.humilimit_low<0) logger.config.humilimit_low=0;
      if(logger.config.humilimit_high<0) logger.config.humilimit_high=0;
      if(logger.config.humilimit_low>100) logger.config.humilimit_low=100;
      if(logger.config.humilimit_high>100) logger.config.humilimit_high=100;
      
      mes.putShort(0x56,(short)(logger.config.humilimit_low*100));
      mes.putShort(0x58,(short)(logger.config.humilimit_high*100));
    } else {
      mes.putShort(0x56,(short)0);
      mes.putShort(0x58,(short)0);
    }
    if((logger.config.alarmtyp&Logger.ALARM_P)!=0) {
      if(logger.config.preslimit_low>logger.config.preslimit_high) {
        float t=logger.config.preslimit_low;
	logger.config.preslimit_low=logger.config.preslimit_high;
	logger.config.preslimit_high=t;
      }
      if(logger.config.preslimit_low<300) logger.config.preslimit_low=300;
      if(logger.config.preslimit_high<300) logger.config.preslimit_high=300;
      if(logger.config.preslimit_low>1200) logger.config.preslimit_low=1200;
      if(logger.config.preslimit_high>1200) logger.config.preslimit_high=1200;
      mes.putShort(0x5a,(short)logger.config.preslimit_low);
      mes.putShort(0x5c,(short)logger.config.preslimit_high);
    } else {
      mes.putShort(0x5a,(short)0);
      mes.putShort(0x5c,(short)0);
    }

    /* Einheit für Lichtstaerke oder Temperatur (bei DL-240K)
    
      0 = LUX   oder C
      1 = FG    oder F
     */
    if(logger.config.temp_unit>1 || logger.config.temp_unit<0) logger.config.temp_unit=0;
    mes.put(0x5e,(byte)logger.config.temp_unit);
    mes.put(0x5f,(byte)0);
    
    /* Einheit für Luftdruck-Alarm 
    
      0 = hPa
      1 = mmHg
      2 = kPa
      
    */
    if(logger.config.pres_unit<0 || logger.config.pres_unit>2) logger.config.pres_unit=0;
    mes.put(0x60,(byte)logger.config.pres_unit);

    mes.putShort(0x61,(short)logger.config.time_year);
    mes.put(0x63,logger.config.time_mon);
    mes.put(0x64,logger.config.time_mday);
    mes.put(0x65,logger.config.time_hour);
    mes.put(0x66,logger.config.time_min);
    mes.put(0x67,logger.config.time_sec);
   /* Datumsformat:
        1  --- YYYY-MM-DD
        2  --- DD-MM-YYYY
        3  --- MM-DD-YYYY
   */
    if(logger.config.date_format<0 || logger.config.date_format>3)  logger.config.date_format=1;
    mes.put(0x68,(byte)logger.config.date_format);
    
    
    mes.put(0x69,(byte)0);
    
    if((logger.config.displaystate&Logger.DISPLAY_ON)!=0)
      mes.put(0x6a,(byte)1);  /* LCD Display */
    else  mes.put(0x6a,(byte)0);
    
   /* Bedingungen für Aufzeichnungsende 
   (00=keine,  01=Stopp-Taste, 02=nach PDF-Erstellung)*/
    flags=0;
    
    if((logger.config.stopcondition&Logger.STOP_BUTTON)!=0) flags|=1; 
    if((logger.config.stopcondition&Logger.STOP_PDF)!=0) flags|=2; 
    mes.put(0x6b,flags);
    
    mes.putShort(0x6c,(short)logger.config.starttime_year);  /* Start Zeit */
    mes.put(0x6e,(byte)logger.config.starttime_mon);
    mes.put(0x6f,(byte)logger.config.starttime_mday);
    mes.put(0x70,(byte)logger.config.starttime_hour);
    mes.put(0x71,(byte)logger.config.starttime_min);
    mes.put(0x72,(byte)logger.config.starttime_sec);

    mes.putShort(0x73,(short)logger.config.stoptime_year);  /* Stop Zeit */
    mes.put(0x75,(byte)logger.config.stoptime_mon);
    mes.put(0x76,(byte)logger.config.stoptime_mday);
    mes.put(0x77,(byte)logger.config.stoptime_hour);
    mes.put(0x78,(byte)logger.config.stoptime_min);
    mes.put(0x79,(byte)logger.config.stoptime_sec);
    
    mes.put(0x7a,(byte)(logger.config.time_offset/60));  /* Startverzögerung in Minuten */

    mes.putShort(0x7b,(short)logger.config.loggerid);  /* Logger ID */


    /*  Time-Format
        0 -- (auto ?)
        1 -- 12h
	2 -- 24h
    */
    if(logger.config.time_format<0 || logger.config.time_format>2) logger.config.time_format=2;
    mes.put(0x7d,(byte)logger.config.time_format);

    mes.put(0x80,(byte)0x3f); /* Field Marker */
    mes.put(0x81,(byte)0x37); /* Field id */
    mes.put(0x82,(byte)0x12); /* Field index */
    
    /*  Sprache für pdf Bericht:
        1 -- german 
        2 -- english
	3 -- frensh
    */
    
    mes.put(0x83,(byte)logger.config.language); 
    
    /* 0x84: bis 0x89:  
       Dateinamenmuster für pdf Bericht.
	0 -- nix
	1 -- Model Name
	2 -- Serial ID
	3 -- Date
	4 -- Time
	5 -- Location
        6 -- Owner
    */
    String[] sep=logger.config.Pdfname.split("-");
    for(int i=0;i<6; i++) {
      if(i<sep.length) {
        if(sep[i].equals("0")) mes.put(0x84+i,(byte)Logger.PDFNAME_NONE);
	else if(sep[i].equals("1") || sep[i].equalsIgnoreCase("M")) mes.put(0x84+i,(byte)Logger.PDFNAME_MODEL);
	else if(sep[i].equals("2") || sep[i].equalsIgnoreCase("S")) mes.put(0x84+i,(byte)Logger.PDFNAME_SERIAL);
	else if(sep[i].equals("3") || sep[i].equalsIgnoreCase("D")) mes.put(0x84+i,(byte)Logger.PDFNAME_DATE);
	else if(sep[i].equals("4") || sep[i].equalsIgnoreCase("T")) mes.put(0x84+i,(byte)Logger.PDFNAME_TIME);
	else if(sep[i].equals("5") || sep[i].equalsIgnoreCase("L")) mes.put(0x84+i,(byte)Logger.PDFNAME_LOCATION);
	else if(sep[i].equals("6") || sep[i].equalsIgnoreCase("O")) mes.put(0x84+i,(byte)Logger.PDFNAME_OWNER);
	else mes.put(0x84+i,(byte)Logger.PDFNAME_NONE);
      } else mes.put(0x84+i,(byte)Logger.PDFNAME_NONE);
    }

    /* Make sure, the first part is always the model name... */
    
    mes.put(0x84,(byte)Logger.PDFNAME_MODEL);


    mes.put(0xb9,(byte)logger.config.pause);  /* Pause function */
    
    n=logger.config.getname();
    int l=n.length();
    if(l>48) l=48;
    
    mes.position(0x8b);
    mes.put(n.getBytes(),0,l);
    mes.putChar(0x8b+l,(char) 0);
    
    mes.put(0xc0,(byte)0x3f);  /* Owner */
    mes.put(0xc1,(byte)0x21);
    mes.put(0xc2,(byte)0x13);
    mes.position(0xc3);
    if(logger.config.Owner!=null) n=logger.config.Owner;
    else n="0";
    l=n.length();
    if(l>32) l=32;
    mes.put(n.getBytes(),0,l);
    mes.putChar(0xc3+l,(char) 0);

    mes.put(0x100,(byte)0x3f);  /* Location */
    mes.put(0x101,(byte)0x20);
    mes.put(0x102,(byte)0x14);
    mes.position(0x103);
    if(logger.config.Location!=null) n=logger.config.Location;
    else n="0";
    l=n.length();
    if(l>32) l=32;
    
    mes.put(n.getBytes(),0,l);
    mes.putChar(0x103+l,(char) 0);
    
    mes.put(0x140,(byte)0x3f);  /* ReportTitle */
    mes.put(0x141,(byte)0x29);
    mes.put(0x142,(byte)0x15);
    mes.position(0x143);
    if(logger.config.Report!=null) n=logger.config.Report;
    else n="0";
    l=n.length();
    if(l>40) l=40;
    mes.put(n.getBytes(),0,l);
    mes.putChar(0x143+l,(char) 0);
    
    mes.put(0x180,(byte)0x3f);  /* Comment */
    mes.put(0x181,(byte)0x32);
    mes.put(0x182,(byte)0x16);
    mes.position(0x183);
    if(logger.config.Comment!=null) {
      n=logger.config.Comment;
      l=n.length();
      if(l>50) l=50;
      mes.put(n.getBytes(),0,l);
      mes.putChar(0x183+l,(char) 0);
    }
    
    mes.put(0x1c0,(byte)0x3f);   /* Comment (Fortsetzung) */
    mes.put(0x1c1,(byte)0x14);
    mes.put(0x1c2,(byte)0x17);
    if(logger.config.Comment2!=null) {
      mes.position(0x1c3);
      n=logger.config.Comment2;
      l=n.length();
      if(l>32) l=32;
      mes.put(n.getBytes(),0,l);
      mes.putChar(0x1c3+l,(char) 0);
    }
    return mes.array();
  }
	
  public static int chk_conf(LoggerConfig config, int loggertype, int memory_size) {
    String n=config.getname();
    if(n.length()==0) return Error.ERR_CNONAME;
    if(n.length()>16) return Error.ERR_CNAME;
    if(config.set_interval>918000 ||config.set_interval<=0) return Error.ERR_INTERVAL;
    
    /* TODO: Check if Start and Stop Time make sense 
      Endzeit muss hinter der Startzeit liegen.
     */
     
    /* TODO: Check if delay makes sense 
    Geben Sie einen Wert zwischen 0 und 60 ein.*/

    /* TODO: Check if interval is in range */


    /* TODO: Check if Limits make sense 
       Hoher Alarm muss größer sein als Niedriger Alarm!*/

    /* TODO: Check if pdf filename is set correctly */

    /* TODO: Check if language, date format and time format are OK */
    
    return Error.OK;
  }


  /* No use? We build a generic configblock of size 512 Bytes.
   */
  public static byte[] read_configblock(Logger logger) {
    return build_conf(logger);
  }


  /* This will read the config part of the medialoggers configfile.
   */

  public static int get_config(Logger logger) {
    byte cbuf[]=read_configblock(logger);
    int ret=Error.OK;
    LoggerConfig config=logger.config;
    config.mglobal_message="";
    config.flag_bits=0;
    config.time_delay=0;
    logger.set_calibration((float)0.01,(float)0,(float)0.01,(float)0,(float)0.1,(float)0);
    config.setname(logger.Serial_id);
    config.config_begin=0x3f;
    config.config_end=0x00;
    config.num_data_conf=(65536-512)/6;

    if(logger.Path==null) {
      loggerstatus="Config read ERROR.";
      return(Error.ERR);
    }
    loggerstatus="reading ...";
    logger.updateMessage("reading ...",0);
    logger.config.alarmtyp=0;

    XmlPullParser parser= Xml.newPullParser();
    InputStream in=null;
    if(logger.Configfilecontent==null) { // parse from file
      String dirname=logger.Path.substring(8); /* Omit the "file://" part*/
      try {
        File file= new File(dirname+ File.separator + logger.Configfile);
        Log.d(TAG,"Read config file: "+file.toString());
	in= new FileInputStream(file);
	if(in!=null) {
          // prepare the file for reading
	  InputStreamReader inputreader = new InputStreamReader(in);
          parser.setInput(inputreader);
        } else {
          loggerstatus="Config read ERROR.";
          ret=Error.ERR;
	  return(ret);
        }
      }
      catch (FileNotFoundException e)  {Log.e(TAG,"ERROR: filenotfound ");return(ret);}
      catch (XmlPullParserException e) {Log.e(TAG,"ERROR: XML error ");   return(ret);} 
    } else {  // parse from string
      try { parser.setInput(new StringReader(logger.Configfilecontent)); }
      catch (XmlPullParserException e) {Log.e(TAG,"ERROR: XML error ");   return(ret);}
    }
    try {
	int eventType = parser.getEventType();
	boolean done = false;
	int parserstate=0;
	while(eventType != XmlPullParser.END_DOCUMENT && !done) {
	  String name = null;
	  switch (eventType){
	  case XmlPullParser.START_DOCUMENT:
	    parserstate=0;
	    break;
          case XmlPullParser.START_TAG:
	    name = parser.getName();
	    if(name.equalsIgnoreCase("recipe")) {
	      parserstate=10;
	      Log.d(TAG,"recipe "+parser.getAttributeName(0)+" = "+parser.getAttributeValue(0));
	    } 
	    else if(name.equalsIgnoreCase("name"))            ;
	    else if(name.equalsIgnoreCase("document"))        parserstate=20;
	    else if(name.equalsIgnoreCase("SerialId"))        parserstate=30;
	    else if(name.equalsIgnoreCase("ProfileId"))       parserstate=40;
	    else if(name.equalsIgnoreCase("FirmwareVersion")) parserstate=50;
	    else if(name.equalsIgnoreCase("Descriptions"))    parserstate=0;
	    else if(name.equalsIgnoreCase("ReportTitle"))     parserstate=250;
	    else if(name.equalsIgnoreCase("GeneralInfo"))     parserstate=260;
	    else if(name.equalsIgnoreCase("Location"))        parserstate=230;
	    else if(name.equalsIgnoreCase("Owner"))           parserstate=240;
	    else if(name.equalsIgnoreCase("ChannelCount"))    parserstate=0;
	    else if(name.equalsIgnoreCase("ChannelList"))     parserstate=0;
	    else if(name.equalsIgnoreCase("Channel"))         parserstate=60;
	    else if(name.equalsIgnoreCase("Index"))           parserstate=70;
	    else if(name.equalsIgnoreCase("ChannelType"))     parserstate=80;
	    else if(name.equalsIgnoreCase("DataCount"))       parserstate=90;
	    else if(name.equalsIgnoreCase("StartMode"))       parserstate=100;
	    else if(name.equalsIgnoreCase("DateFormat"))      parserstate=130;
	    else if(name.equalsIgnoreCase("TemperatureUnit")) parserstate=140;
	    else if(name.equalsIgnoreCase("PressureUnit"))    parserstate=150;
	    else if(name.equalsIgnoreCase("SampleRate"))      parserstate=110;
	    else if(name.equalsIgnoreCase("StarTime"))        parserstate=120;
	    else if(name.equalsIgnoreCase("MinLimitTem"))     parserstate=300;
	    else if(name.equalsIgnoreCase("MaxLimitTem"))     parserstate=310;
	    else if(name.equalsIgnoreCase("MinLimitRh"))      parserstate=320;
	    else if(name.equalsIgnoreCase("MaxLimitRh"))      parserstate=330;
	    else if(name.equalsIgnoreCase("MinLimiPressure")) parserstate=340;
	    else if(name.equalsIgnoreCase("MaxLimiPressure")) parserstate=350;
	    else if(name.equalsIgnoreCase("CodedData"))       parserstate=0;
	    else Log.d(TAG,"Xmltag: "+name);
	    break;
	  case XmlPullParser.TEXT:
	    if(parserstate<=10) ;
	    else if(parserstate==11) {
	      Log.d(TAG,"Loggertypname = "+parser.getText());
	      logger.Product=parser.getText();
	    } 
	    else if(parserstate<=20) ;
	    else if(parserstate==21) {
	      Log.d(TAG,"Messungname = "+parser.getText());
	      logger.config.setname(parser.getText());
	    }
	    else if(parserstate<30) ;
	    else if(parserstate==30) {
	      logger.Serial_id=parser.getText();
	      Log.d(TAG,"SerialID = "+parser.getText());
	    }
	    else if(parserstate<40) ;
	    else if(parserstate==40) {
	      try {
	        logger.config.loggerid=(int)Double.parseDouble(parser.getText());
	      } catch (NumberFormatException e) {logger.config.loggerid=4711;}
	      Log.d(TAG,"ProfileID = "+parser.getText());
	    }
	    else if(parserstate<50) ;
	    else if(parserstate==50) {
	      logger.FirmwareVersion=parser.getText();
	      Log.d(TAG,"FirmwareVersion = "+parser.getText());
	    } 
	    else if(parserstate<=60) ;
	    else if(parserstate==61) {
	      Log.d(TAG,"ChannelName = "+parser.getText());
	    } 
	    else if(parserstate<70) ;
	    else if(parserstate==70) {
	      Log.d(TAG,"ChannelIndex = "+parser.getText());
	    } 
	    else if(parserstate<80) ;
	    else if(parserstate==80) {
	      Log.d(TAG,"ChannelType = "+parser.getText());
	    } 
	    else if(parserstate<90) ;
	    else if(parserstate==90) {
	      Log.d(TAG,"DataCount = "+parser.getText());
	      logger.config.num_data_rec=Integer.parseInt(parser.getText());
	    } 
	    else if(parserstate<100) ;
	    else if(parserstate==100) {
	      Log.d(TAG,"StartMode = "+parser.getText());
	      int a=Integer.parseInt(parser.getText());
	      switch(a) {
	        case 0: 
		  logger.config.startcondition=Logger.START_INSTANT;
		  logger.config.stopcondition|=Logger.STOP_MEMFULL;
		  break;
		case 1:
	          logger.config.startcondition=Logger.START_BUTTON;
		  break;
		case 2:
		  logger.config.startcondition=Logger.START_TIME;
		  logger.config.stopcondition&=(~Logger.STOP_MEMFULL);
		  break;
		case 3:
		  logger.config.startcondition=Logger.START_TIME;
		  logger.config.stopcondition|=Logger.STOP_MEMFULL;
		  break;
		case 4:
		  logger.config.startcondition=Logger.START_INSTANT;
		  logger.config.stopcondition&=(~Logger.STOP_MEMFULL);
		  break;
	      }
	    } 
	    else if(parserstate<110) ;
	    else if(parserstate==110) {
	      Log.d(TAG,"SampleRate = "+parser.getText());
	      logger.config.got_interval=Integer.parseInt(parser.getText());
	      logger.config.set_interval=logger.config.got_interval;
	    } 
	    else if(parserstate<120) ;
	    else if(parserstate==120) {
	      Log.d(TAG,"StarTime = "+parser.getText());
	      byte[] data = Base64.decode(parser.getText(), Base64.DEFAULT);
	      logger.config.time_year=data[0]+1980;
	      logger.config.time_mon= data[1];
	      logger.config.time_mday=data[2];
              logger.config.time_hour=data[3];
              logger.config.time_min= data[4];
	      logger.config.time_sec= data[5];
	    } 
	    else if(parserstate<130) ;
	    else if(parserstate==130) {
	      Log.d(TAG,"DateFormat = "+parser.getText());
	      logger.config.date_format=Integer.parseInt(parser.getText());
	    }
	    else if(parserstate<140) ;
	    else if(parserstate==140) {
	      Log.d(TAG,"TemperatureUnit = "+parser.getText());
	      logger.config.temp_unit=Integer.parseInt(parser.getText());
	    }
	    else if(parserstate<150) ;
	    else if(parserstate==150) {
	      Log.d(TAG,"PressureUnit = "+parser.getText());
	      logger.config.pres_unit=Integer.parseInt(parser.getText());
	    }
	    else if(parserstate<230) ;
	    else if(parserstate==230) {
	      Log.d(TAG,"Location = "+parser.getText());
	      logger.config.Location=parser.getText();
	      if(logger.config.Location==null || logger.config.Location.equals("\n")) logger.config.Location="0";
	    } 
	    else if(parserstate<240) ;
	    else if(parserstate==240) {
	      Log.d(TAG,"Owner = "+parser.getText());
	      logger.config.Owner=parser.getText();
	      if(logger.config.Owner==null || logger.config.Owner.equals("\n")) logger.config.Owner="";
	    } 
	    else if(parserstate<250) ;
	    else if(parserstate==250) {
	      Log.d(TAG,"Report = "+parser.getText());
	      logger.config.Report=parser.getText();
	      if(logger.config.Report==null || logger.config.Report.equals("\n")) logger.config.Report="0";
	    }
	    else if(parserstate<260) ;
	    else if(parserstate==260) {
	      Log.d(TAG,"Comment = "+parser.getText());
	      logger.config.Comment=parser.getText();
	      if(logger.config.Comment==null || logger.config.Comment.equals("\n")) logger.config.Comment="";
	      if(logger.config.Comment2==null || logger.config.Comment2.equals("\n")) logger.config.Comment2="";
	    } 
	    else if(parserstate<300) ;
	    else if(parserstate==300) {
	      Log.d(TAG,"MinLimitTem = "+parser.getText());
	      byte[] data = Base64.decode(parser.getText(), Base64.DEFAULT);
	      if(data.length>1) {
	        logger.config.templimit_low=(float)((short)(((short)data[0]&0xff)|((short)data[1]&0xff)<<8))/100;
		logger.config.alarmtyp|=Logger.ALARM_T|Logger.ALARM_ON;
	      } else logger.config.templimit_low=0;
	    } 
	    else if(parserstate<310) ;
	    else if(parserstate==310) {
	      Log.d(TAG,"MaxLimitTem = "+parser.getText());
	      byte[] data = Base64.decode(parser.getText(), Base64.DEFAULT);
	      if(data.length>1) {
	        logger.config.templimit_high=(float)((short)(((short)data[0]&0xff)|((short)data[1]&0xff)<<8))/100;
		logger.config.alarmtyp|=Logger.ALARM_T|Logger.ALARM_ON;
	      } else logger.config.templimit_high=0;
	    } 
	    else if(parserstate<320) ;
	    else if(parserstate==320) {
	      Log.d(TAG,"MinLimitHumi = "+parser.getText());
	      byte[] data = Base64.decode(parser.getText(), Base64.DEFAULT);
	      if(data.length>1) {
	        logger.config.humilimit_low=((float)(data[0]&0xff)+(float)(data[1]&0xff)*256)/100;
		logger.config.alarmtyp|=Logger.ALARM_H|Logger.ALARM_ON;
	      } else logger.config.humilimit_low=0;
	    } 
	    else if(parserstate<330) ;
	    else if(parserstate==330) {
	      Log.d(TAG,"MaxLimitHumi = "+parser.getText());
	      byte[] data = Base64.decode(parser.getText(), Base64.DEFAULT);
	      if(data.length>1) {
	        logger.config.humilimit_high=((float)(data[0]&0xff)+(float)(data[1]&0xff)*256)/100;
		logger.config.alarmtyp|=Logger.ALARM_H|Logger.ALARM_ON;
	      }
	    } 
	    else if(parserstate<340) ;
	    else if(parserstate==340) {
	      Log.d(TAG,"MinLimitPres = "+parser.getText());
	      byte[] data = Base64.decode(parser.getText(), Base64.DEFAULT);
	      if(data.length>1) {
	        logger.config.preslimit_low=(float)(data[0]&0xff)+(float)(data[1]&0xff)*256;
		logger.config.alarmtyp|=Logger.ALARM_P|Logger.ALARM_ON;
	      }
	    } 
	    else if(parserstate<350) ;
	    else if(parserstate==350) {
	      Log.d(TAG,"MaxLimitPres = "+parser.getText());
	      byte[] data = Base64.decode(parser.getText(), Base64.DEFAULT);
	      if(data.length>1) {
	        logger.config.preslimit_high=(float)(data[0]&0xff)+(float)(data[1]&0xff)*256;
		logger.config.alarmtyp|=Logger.ALARM_P|Logger.ALARM_ON;
	      }
	    } 
	    else Log.d(TAG,"ps="+parserstate+" TEXT="+parser.getText());
	    parserstate++;
	    break;
	  case XmlPullParser.END_TAG:
	    name = parser.getName();
	    if(name.equalsIgnoreCase("recipe")){
	      parserstate=0;
	      done = true;
	    }
	    else if(name.equalsIgnoreCase("document")) parserstate=0;
	    else if(name.equalsIgnoreCase("Descriptions")) parserstate=0;
	    break;
	  }
	  eventType = parser.next();
	}
    } 
    catch (XmlPullParserException e) {Log.e(TAG,"ERROR: XML error ");   return(ret);} 
    catch (IOException e) {Log.e(TAG,"ERROR: IO error ");   return(ret);} 
    ret=Error.OK;
    logger.isreadconfig=true;
    loggerstatus="Config OK. ";
    logger.updateMessage("Config OK. ",0);
    if(in!=null) {
      try {in.close();} catch (IOException e) {return(ret);}
    }
    return ret;
  }

  /* Write the configuration which is hold in memory into the configuration 
     file (SetLog.bn1) */

  public static int write_configblock(byte[] cbuf,Logger logger) {
    int ret=Error.OK;
    FileOutputStream fOut = null;
    if(logger.isconnected) {
      Log.d(TAG,"writeconfigblock...");
      if(logger.Path==null) {
        loggerstatus="Config ERROR.";
        return(Error.ERR);
      }
      String dirname=logger.Path.substring(8); /* Omit the "file://" part*/
      File file=null;
      File dir=null;
      try {
        dir=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
	if(logger.fakeconfigdir) file= new File(dir,"SetLog.bn1");
	else file= new File(dirname+File.separator+"SetLog.bn1");
        Log.d(TAG,"Write config file: "+file.toString());
	if (!file.exists()) file.createNewFile();
	fOut=new FileOutputStream(file);
        fOut.write(cbuf);
        fOut.flush();
      }
      catch (Throwable thr)  {
        // FilenotFound oder IOException
        Log.e(TAG,"ERROR: config not written. ");
	Toast.makeText(logger.mActivity.getApplicationContext(), "ERROR saving "+file.toString()+" : "+thr.toString()+".", Toast.LENGTH_LONG).show();
	ret=Error.ERR;
      }
      finally {
        try {
          fOut.close();
        } 
	catch (IOException e) {e.printStackTrace();ret=Error.ERR;}
      }
    } else {
      Log.d(TAG, "connection problem ");
      ret=Error.ERR;
    }
    return(ret);
  }




  public static int get_data(Logger logger) {
    int q=0,count=0,blocknr=0,blocksize=0;
    final LoggerData data=logger.data;
    data.quality=0; 
    data.set_calibration((float)0.01,0,(float) 0.01, 0);
    logger.updateProgress(0);
    logger.updateStatus(0);

    if(logger.Path!=null) {
      XmlPullParser parser= Xml.newPullParser();
      InputStream in=null;
      if(logger.Configfilecontent==null) { // parse from file
        String dirname=logger.Path.substring(8); /* Omit the "file://" part*/
        try {
          File file= new File(dirname+ File.separator + logger.Configfile);
          Log.d(TAG,"Read data from file: "+file.toString());
  	  in= new FileInputStream(file);
	  if(in!=null) {
            // prepare the file for reading
	    InputStreamReader inputreader = new InputStreamReader(in);
            parser.setInput(inputreader);
          } else {
            loggerstatus="Config read ERROR.";
            q=Error.ERR;
	    return(q);
          }
        }
        catch (FileNotFoundException e)  {Log.e(TAG,"ERROR: filenotfound ");return(q);}
        catch (XmlPullParserException e) {Log.e(TAG,"ERROR: XML error ");   return(q);} 
      } else {  // parse from string
        try { parser.setInput(new StringReader(logger.Configfilecontent)); }
        catch (XmlPullParserException e) {Log.e(TAG,"ERROR: XML error ");   return(q);}
      }
      String dirname=logger.Path.substring(8); /* Omit the "file://" part*/
      try { 
	
	  int eventType = parser.getEventType();
	  boolean done = false;
	  int parserstate=0;
	  while(eventType != XmlPullParser.END_DOCUMENT && !done) {
	    String name = null;
	    switch (eventType){
	      case XmlPullParser.START_DOCUMENT:
	        parserstate=0;
		break;
	      case XmlPullParser.START_TAG:
		name = parser.getName();
		if (name.equalsIgnoreCase("CodedData")) {
		  Log.d(TAG,"codeddata  "+parser.getAttributeName(0)+" = "+parser.getAttributeValue(0));
		  Log.d(TAG,"codeddata2 "+parser.getAttributeName(1)+" = "+parser.getAttributeValue(1));
		  parserstate=1;
		  blocknr=Integer.parseInt(parser.getAttributeValue(1));
		  blocksize=Integer.parseInt(parser.getAttributeValue(0));
		} 
		break;
	      case XmlPullParser.TEXT:
	        if(parserstate==0) ;
                else if(parserstate==1) {
		  Log.d(TAG,"ps="+parserstate+" TEXT="+parser.getText());
		  Log.d(TAG,"idx="+blocknr+" len="+blocksize);
		  byte[] buf = Base64.decode(parser.getText(), Base64.DEFAULT);

		  for(int j=0;j<blocksize;j++) {
		    if(logger.loggertype==Logger.LTYP_THP) {
		      data.add(
		        (short) ((short)buf[j*6+0]&0xff | ((short)buf[j*6+1]&0xff)<<8),
		        (short) ((short)buf[j*6+2]&0xff | ((short)buf[j*6+3]&0xff)<<8),
			(short) ((short)buf[j*6+4]&0xff | ((short)buf[j*6+5]&0xff)<<8));
		    } else if(logger.loggertype==Logger.LTYP_TH) {
		      data.add((short) ((short)buf[j*4]&0xff | ((short)buf[j*4+1]&0xff)<<8),
			       (short) ((short)buf[j*4+2]&0xff | ((short)buf[j*4+3]&0xff)<<8));

		    } else {
		      data.add((short) ((short)buf[j*2]&0xff | ((short)buf[j*2+1]&0xff)<<8));
		    }
	          }
		  parserstate=0;
		  count+=blocksize;
		  logger.updateProgress((int)(count*100.0/logger.config.num_data_rec));
		} 
		else Log.d(TAG,"ps="+parserstate+" TEXT="+parser.getText());
		break;
	      case XmlPullParser.END_TAG:
		name = parser.getName();
		if (name.equalsIgnoreCase("recipe")){
		  parserstate=0;
		  done = true;
		} else if (name.equalsIgnoreCase("document")) parserstate=0;
		break;
	      }
	      eventType = parser.next();
	    }
          logger.isreaddata=true;
          if(in!=null) { // close the file again
            try {in.close();} catch (IOException e) {return(q);}
          }
      } 
      catch (FileNotFoundException e)  {Log.e(TAG,"ERROR: filenotfound ");return(q);} 
      catch (XmlPullParserException e) {Log.e(TAG,"ERROR: XML error ");return(q);} 
      catch (IOException e)            {Log.e(TAG,"ERROR: IO error ");return(q);}	    
    } else loggerstatus="Data read ERROR.";
    logger.updateProgress(100);

    /* Check data quality */
    
    if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) {
      for(int i=0;i<data.anzdata;i++) {
	if(data.get_temp(i)>200 || data.get_temp(i)<-100 || 
	   data.get_rh(i)>104 || data.get_rh(i)<-1) q++;    		
      }
    }
    return(q);
  }

  public static String get_status(Logger logger) {
    String ret=loggerstatus; 
    if(logger.isreadconfig) {
      ret=logger.typestring()+": typ="+logger.loggertype;
      if(logger.config.config_begin==0) ret=ret+": normal,";
      else ret=ret+": manual/delayed started, ";
      if(logger.config.num_data_rec==0) ret=ret+" NO DATA";
      else ret=ret+" DATA available ("+logger.config.num_data_rec+")";
    } else ret="Config has not yet been read";
    return ret+".";
  }
}
