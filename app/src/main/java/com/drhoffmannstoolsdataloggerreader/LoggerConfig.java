package com.drhoffmannstoolsdataloggerreader;

/* LoggerConfig.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.app.Activity;
import android.util.Log;

public class LoggerConfig {
  private static final String TAG = "LCONF"; 
  public static final String FTYPE_CONFIG = ".vdlcfg";  

  public int block_type=0; 
  /*1=EL-USB-1 Temperature Logger Packet size 64 bytes
    2=EL-USB-1 Temperature Logger Packet size 64 bytes
    3=EL-USB-2 Temperature/Humidity Logger Packet size 128 bytes
    4=EL-USB-3 Voltage Logger 256 bytes
    5=EL-USB-4 Current Logger 256 bytes
    6=EL-USB-3 Voltage Logger 256 bytes
    7=EL-USB-4 Current Logger 256 bytes
    8=EL_USB_LITE Temperature logger 256 bytes
    9=EL_USB_CO Carbon Monoxide Logger 256 bytes
   10=EL_USB_TC Thermocouple Temperature Logger 256 bytes
   11=EL-USB-CO300 Low Range Carbon Monoxide Logger 256 bytes
   12=EL-USB-2-LCD 256 bytes
   13=EL-USB-2+ 256 bytes
   es fehlen noch: EL_USB-RT (evtl ganz anderes Protokoll, gar kein Datenlogger!) und EL_USB-5
  */
  public int startcondition=Logger.START_INSTANT;  /* */
  public int stopcondition=Logger.STOP_BUTTON;  /* */
  public int alarmtyp=Logger.ALARM_OFF;
  public int displaystate=Logger.DISPLAY_ON;
  public int pause=0;
  public int date_format=Logger.DATEFORMAT_YYYYMMDD;
  public float templimit_low=15;
  public float templimit_high=30;
  public float humilimit_low=40;
  public float humilimit_high=60;
  public float preslimit_low=700;
  public float preslimit_high=1100;
  public int starttime_year=2020; /* start time */
  public int starttime_mon=1; 
  public int starttime_mday=1;
  public int starttime_hour=12;
  public int starttime_min=0;
  public int starttime_sec=0;
  public int stoptime_year=2020; /* start time */
  public int stoptime_mon=1; 
  public int stoptime_mday=1;
  public int stoptime_hour=12;
  public int stoptime_min=0;
  public int stoptime_sec=0;
  public int bytestoread=0;
  public int block_cmd=0;  /* */
  public long time_offset=0; /* Number of samples (?) or seconds ? to ignore before data taking starts.*/
  public long time_delay=0;  /* Number of Seconds where the button was pressed and the delayed data taking started.*/
  public int time_format=Logger.TIMEFORMAT_24H;
  public int temp_unit=Logger.UNIT_C;
  public int pres_unit=Logger.UNIT_HPA;
  public int language=Logger.LANGUAGE_GERMAN;

  public float temp_offset=0; /* Freetec white erlaubt offsets*/
  public float humi_offset=0;
  public float pres_offset=0;

  public short flag_bits=0; /*
*        x xxxx xxxx xxxx
*                    |||+--HIGH_ALARM_STATE: 1 = High Alarm Enabled, 0 = High Alarm Disabled
*                    ||+---LOW_ALARM_STATE:  1 = Low Alarm Enabled, 0 = Low Alarm Disabled
*                    |+----HIGH_ALARM_LATCH: 1 = High Alarm Indication Hold Enabled, 0 disabled
*                    +-----LOW_ALARM_LATCH:  1 = Low Alarm Indication Hold Enabled, 0 disabled
*                  +-------CH2_HIGH_ALARM_STATE  EL-USB-2 Only
*                 +--------CH2_LOW_ALARM_STATE   EL-USB-2 Only
*                +---------CH2_HIGH_ALARM_LATCH  EL-USB-2 Only
*               +----------CH2_LOW_ALARM_LATCH   EL-USB-2 Only
*             +------------LOGGING_STATE:    1 = Delayed Start or Logging,  0 = Off
*            +-------------UNREAD:           1 = Data has NOT been downloaded.
*           +--------------BATTERY_LOW:      1 = During the last acquistion battery level dropped to a low level 
                   //     and logging stopped
*          +---------------BATTERY_FAIL:     1= During the last acquistion battery level dropped to a critical level 
                   //     and logging stopped
*        +-----------------SENSOR_FAIL:      1 = During last acquisition there were sensor errors. EL-USB-2 Only

*/ 
  public int flag_bits2=0;
  public int rollover_count=0; // wie oft der Ringbuffer von vorne begonnen hat.
  public int sensor_type=0;
  public int factory_use=0;
  /* Parameter für DL-2?0-T* */
  public int loggerid=4711;
  public String Owner="";
  public String Location="";
  public String Report="";
  public String Comment="";
  public String Comment2="";
  public String Pdfname="1-3-4-0-0-0";
	
  public float scaling_factor=0;  // Cap Scaling factor
  public short battery_alarm=0;

  public float calibration_Mvalue=(float)1.0; /*3.4E +/- 38 (7dig)*/
  public float calibration_Cvalue=(float)-40; /*3.4E +/- 38 (7dig)*/
  public int rawinputreading=0; /* */
  public long serial_number=0;    /* */
  public char[] version=new char[4]; /* EL-USB-1,2,3*/
  public char[] unit=new char[12]; /*EL-USB-3*/
  public char[] cal1=new char[8]; /*EL-USB-3*/
  public char[] cal2=new char[8]; /*EL-USB-3*/
  public char[] cal3=new char[8]; /*EL-USB-3*/
  public char[] cal4=new char[8]; /*EL-USB-3*/
  public char[] halmtxt=new char[8]; /*EL-USB-3*/
  public char[] lalmtxt=new char[8]; /*EL-USB-3*/
  public char[] defrange=new char[14]; /*EL-USB-3*/
  public char[] defunit=new char[12]; /*EL-USB-3*/
  public char[] defiunit=new char[12]; /*EL-USB-3*/
  public char[] defcal1=new char[8]; /*EL-USB-3*/
  public char[] defcal2=new char[8]; /*EL-USB-3*/
  public char[] defcal3=new char[8]; /*EL-USB-3*/
  public char[] defcal4=new char[8]; /*EL-USB-3*/
  public char[] defhalmtxt=new char[8]; /*EL-USB-3*/
  public char[] deflalmtxt=new char[8]; /*EL-USB-3*/

  public String mglobal_message="";

/*Spezielle Parameter füer die Voltcraft-Reihe*/
  public long timestamp; /* Zeit, wann die Config gelesen wurde .*/
  public int  config_begin; /* 0xce = set config, 0x00 = logger is active, other values ader time_delay */  

/*Allgemeine Parameter (angeleht an Voltcraft)*/

/*  4- 7 */public  int num_data_conf=10800; /* number of data configured */
/*  8-11 */public  int num_data_rec; /* number of data recorded */
/* 12-15 */ int got_interval=60;  /* log interval in seconds, read from the logger */
            int set_interval=60;  /* interval in seconds to be set into the logger */
/* 16-19 */ int time_year=2020;
/* 20-21    short  padding20; */
/* 22-23    thresh_temp_low */ 
/* 24-25    short  padding24; */
/* 26-27    thresh_temp_high*/ 
/* 28    */ byte time_mon=1; /* start time, local (!) timezone */
/* 29    */ byte time_mday=1;
/* 30    */ byte time_hour=12;
/* 31    */ byte time_min=0;
/* 32    */ byte time_sec=0;
/* 33    temp_is_fahrenheit */ 
/* 34    */ byte led_conf=10; /* bit 0: alarm on/off, bits 1-2: 10 (?), bits 3-7: flash frequency in seconds */
// 35 ?!
/* 35-50 */ public char name[]=new char[16]; /* config name. actually just 16 bytes: 35-50 */
/* 51    */ byte start=2; /* 0x02 = start logging immediately; 0x01 = start logging manually (button) */
/* 52-53    short  padding52; //
/* 54-55 thresh_rh_low */ 
/* 56-57    short  padding56; //
/* 58-59 thresh_rh_high */ 
/* 60-63 */ int config_end; /* = config_begin */


	
  public byte[] configbuf=null;
  public int cbufsize=64;
	
  public void setname(String t) {
    int namelen=t.length();
    if(namelen>15) namelen=15;
    int offset=t.length()-namelen;
    if(offset<0) offset=0;
    char un[]=t.toCharArray();
    for(int i=0;i<15;i++) {
    	    if(i<un.length) name[i]=un[i+offset];
    }
    name[namelen]=0;
  }
  public String getname() {
    int i;
    for(i=0;i<name.length;i++) {if(name[i]==0) break;}
    return String.valueOf(name).substring(0,i);
  }
  public int chk_name() {
    	int i;
    	int ret=Error.OK;
    	for(i=0;i<name.length && name[i]!=0;i++) {
    		switch(name[i]) {
    		case '/':
    		case '\\':
    		case '%':
    		case '&':
    		case '*':
    		case '<':
    		case '>':
    		case '?':
    		case '|':
       		case '(':
       		case ')':
    		case '\r':
    		case '\n':
    			ret=Error.ERR_ILLNAME;
    			break;
    		}
    	}
    	if(name[0]==0) ret=Error.ERR_CNONAME;
    	return ret;
    }
    
    
    public String getunit() {
    	int i;
    	for(i=0;i<unit.length;i++) {if(unit[i]==0) break;}
    	return String.valueOf(unit).substring(0,i);
    }
    public void setunit(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<unit.length;i++) {if(i<un.length) unit[i]=un[i];}
    	if(i<unit.length) unit[i]=0;
    }
    public String getcal1() {
    	int i;
    	for(i=0;i<cal1.length;i++) {if(cal1[i]==0) break;}
    	return String.valueOf(cal1).substring(0,i);
    }
    public void setcal1(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<cal1.length;i++) {if(i<un.length) cal1[i]=un[i];}
    	if(i<cal1.length) cal1[i]=0;
    }
    
    public String getcal2() {
    	int i;
    	for(i=0;i<cal2.length;i++) {if(cal2[i]==0) break;}
    	return String.valueOf(cal2).substring(0,i);
    }
    public void setcal2(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<cal2.length;i++) {if(i<un.length) cal2[i]=un[i];}
    	if(i<cal2.length) cal2[i]=0;
    }
    public String getcal3() {
    	int i;
    	for(i=0;i<cal3.length;i++) {if(cal3[i]==0) break;}
    	return String.valueOf(cal3).substring(0,i);
    }
    public void setcal3(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<cal3.length;i++) {if(i<un.length) cal3[i]=un[i];}
    	if(i<cal3.length) cal3[i]=0;
    }
    public String getcal4() {
    	int i;
    	for(i=0;i<cal4.length;i++) {if(cal4[i]==0) break;}
    	return String.valueOf(cal4).substring(0,i);
    }
    public void setcal4(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<cal4.length;i++) {if(i<un.length) cal4[i]=un[i];}
    	if(i<cal4.length) cal4[i]=0;
    }
    public String gethalmtxt() {
    	int i;
    	for(i=0;i<halmtxt.length;i++) {if(halmtxt[i]==0) break;}
    	return String.valueOf(halmtxt).substring(0,i);
    }
    public void sethalmtxt(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<halmtxt.length;i++) {if(i<un.length) halmtxt[i]=un[i];}
    	if(i<halmtxt.length) halmtxt[i]=0;
    }
    public String getlalmtxt() {
    	int i;
    	for(i=0;i<lalmtxt.length;i++) {if(lalmtxt[i]==0) break;}
    	return String.valueOf(lalmtxt).substring(0,i);
    }
    public void setlalmtxt(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<lalmtxt.length;i++) {if(i<un.length) lalmtxt[i]=un[i];}
    	if(i<lalmtxt.length) lalmtxt[i]=0;
    }
 
    public String getdefrange() {
    	int i;
    	for(i=0;i<defrange.length;i++) {if(defrange[i]==0) break;}
    	return String.valueOf(defrange).substring(0,i);
    }
    public void setdefrange(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<defrange.length;i++) {if(i<un.length) defrange[i]=un[i];}
    	if(i<defrange.length) defrange[i]=0;
    }
    public String getdefunit() {
    	int i;
    	for(i=0;i<defunit.length;i++) {if(defunit[i]==0) break;}
    	return String.valueOf(defunit).substring(0,i);
    }
    public void setdefunit(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<defunit.length;i++) {if(i<un.length) defunit[i]=un[i];}
    	if(i<defunit.length) defunit[i]=0;
    }
    
    public String getdefiunit() {
    	int i;
    	for(i=0;i<defiunit.length;i++) {if(defiunit[i]==0) break;}
    	return String.valueOf(defiunit).substring(0,i);
    }
    public void setdefiunit(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<defiunit.length;i++) {if(i<un.length) defiunit[i]=un[i];}
    	if(i<defiunit.length) defiunit[i]=0;
    }
   
    
    
    
    public String getdefcal1() {
    	int i;
    	for(i=0;i<defcal1.length;i++) {if(defcal1[i]==0) break;}
    	return String.valueOf(defcal1).substring(0,i);
    }
    public void setdefcal1(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<defcal1.length;i++) {if(i<un.length) defcal1[i]=un[i];}
    	if(i<defcal1.length) defcal1[i]=0;
    }
    
    public String getdefcal2() {
    	int i;
    	for(i=0;i<defcal2.length;i++) {if(defcal2[i]==0) break;}
    	return String.valueOf(defcal2).substring(0,i);
    }
    public void setdefcal2(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<defcal2.length;i++) {if(i<un.length) defcal2[i]=un[i];}
    	if(i<defcal2.length) defcal2[i]=0;
    }
    public String getdefcal3() {
    	int i;
    	for(i=0;i<defcal3.length;i++) {if(defcal3[i]==0) break;}
    	return String.valueOf(defcal3).substring(0,i);
    }
    public void setdefcal3(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<defcal3.length;i++) {if(i<un.length) defcal3[i]=un[i];}
    	if(i<defcal3.length) defcal3[i]=0;
    }
    public String getdefcal4() {
    	int i;
    	for(i=0;i<defcal4.length;i++) {if(defcal4[i]==0) break;}
    	return String.valueOf(defcal4).substring(0,i);
    }
    public void setdefcal4(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<defcal4.length;i++) {if(i<un.length) defcal4[i]=un[i];}
    	if(i<defcal4.length) defcal4[i]=0;
    }
    public String getdefhalmtxt() {
    	int i;
    	for(i=0;i<defhalmtxt.length;i++) {if(defhalmtxt[i]==0) break;}
    	return String.valueOf(defhalmtxt).substring(0,i);
    }
    public void setdefhalmtxt(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<defhalmtxt.length;i++) {if(i<un.length) defhalmtxt[i]=un[i];}
    	if(i<defhalmtxt.length) defhalmtxt[i]=0;
    }
    public String getdeflalmtxt() {
    	int i;
    	for(i=0;i<deflalmtxt.length;i++) {if(deflalmtxt[i]==0) break;}
    	return String.valueOf(deflalmtxt).substring(0,i);
    }
    public void setdeflalmtxt(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<deflalmtxt.length;i++) {if(i<un.length) deflalmtxt[i]=un[i];}
    	if(i<deflalmtxt.length) deflalmtxt[i]=0;
    }
  
    
    
    
  public String getversion() {
    	int i;
    	for(i=0;i<version.length;i++) {if(version[i]==0) break;}
    	return String.valueOf(version).substring(0,i);
  }
  public void setversion(String t) {
    	char un[]=t.toCharArray();
    	int i;
    	for(i=0;i<version.length;i++) {if(i<un.length) version[i]=un[i];}
    	if(i<version.length) version[i]=0;
  }
  public String toString() {
    String mes="# USB Datalogger configuration\n"+
    		"name="+getname()+"\n"+
    		"timestamp="+timestamp+"\n"+
    		"type="+block_type+"\n"+
    		"loggerid="+loggerid+"\n"+
    		"serial="+String.format("0x%08x",(int)serial_number&0xffffffff)+"\n"+
  		"num_data_conf="+num_data_conf+"\n"+
  		"num_data_rec="+num_data_rec+"\n"+
		"interval="+got_interval+"\n"+
		"startcondition="+startcondition+"\n"+
		"stopcondition="+stopcondition+"\n"+
		"alarmtyp="+alarmtyp+"\n"+
		"displaystate="+displaystate+"\n"+
		"date_format="+date_format+"\n"+
		"time_format="+time_format+"\n"+
		"pause="+pause+"\n"+
		"language="+language+"\n"+
		
		"time_year="+time_year+"\n"+
		"time_mon="+(int)time_mon+"\n"+
		"time_mday="+(int)time_mday+"\n"+
		"time_hour="+(int)time_hour+"\n"+
		"time_min="+(int)time_min+"\n"+
		"time_sec="+(int)time_sec+"\n"+

		"starttime_year="+starttime_year+"\n"+
		"starttime_mon=" +starttime_mon+"\n"+
		"starttime_mday="+starttime_mday+"\n"+
		"starttime_hour="+starttime_hour+"\n"+
		"starttime_min=" +starttime_min+"\n"+
		"starttime_sec=" +starttime_sec+"\n"+
		
		"stoptime_year="+stoptime_year+"\n"+
		"stoptime_mon=" +stoptime_mon+"\n"+
		"stoptime_mday="+stoptime_mday+"\n"+
		"stoptime_hour="+stoptime_hour+"\n"+
		"stoptime_min=" +stoptime_min+"\n"+
		"stoptime_sec=" +stoptime_sec+"\n"+
		
		"temp_unit="+temp_unit+"\n"+
		"pres_unit="+pres_unit+"\n"+
		"led_conf="+(int)led_conf+"\n"+
		"unit="+getunit()+"\n"+
		"defunit="+getdefunit()+"\n"+
		"defiunit="+getdefiunit()+"\n"+
		"cal1="+getcal1()+"\n"+
		"cal2="+getcal2()+"\n"+
		"cal3="+getcal3()+"\n"+
		"cal4="+getcal4()+"\n"+
		"halm="+gethalmtxt()+"\n"+
		"lalm="+getlalmtxt()+"\n"+
		"defcal1="+getdefcal1()+"\n"+
		"defcal2="+getdefcal2()+"\n"+
		"defcal3="+getdefcal3()+"\n"+
		"defcal4="+getdefcal4()+"\n"+
		"defhalm="+getdefhalmtxt()+"\n"+
		"deflalm="+getdeflalmtxt()+"\n"+
		"range="+getdefrange()+"\n"+
		"start="+(int)start+"\n"+
		"rollover_count="+rollover_count+"\n"+
		"templimit_high="+templimit_high+"\n"+
		"templimit_low=" +templimit_low+"\n"+
		"humilimit_high="+humilimit_high+"\n"+
		"humilimit_low=" +humilimit_low+"\n"+
		"preslimit_high="+preslimit_high+"\n"+
		"preslimit_low=" +preslimit_low+"\n"+
		"time_delay="+time_delay+"\n"+
		"time_offset="+time_offset+"\n"+
		"calibration_M="+calibration_Mvalue+"\n"+
		"calibration_C="+calibration_Cvalue+"\n"+
		"scaling_factor="+scaling_factor+"\n"+
		"temp_offset="+temp_offset+"\n"+
		"humi_offset="+humi_offset+"\n"+
		"pres_offset="+pres_offset+"\n"+
		"sensor_type="+sensor_type+"\n"+
		"message="+mglobal_message+"\n"+
		"battery_alarm="+battery_alarm+"\n"+
		"config_begin="+config_begin+"\n"+
		"config_end="+config_end+"\n"+
		"flag_bits="+flag_bits+"\n"+
		"flag_bits2="+flag_bits2+"\n"+
    		"owner="+Owner+"\n"+
    		"location="+Location+"\n"+
    		"report="+Report+"\n"+
    		"comment="+Comment+"\n"+
    		"comment2="+Comment2+"\n"+
    		"pdfname="+Pdfname+"\n";
    return mes;
  }
    
  public void save(Activity a) {
    String filename=getname()+LoggerConfig.FTYPE_CONFIG;
    FileOutputStream fos=null;
    OutputStreamWriter osw=null;
    File mConfigPath= new File(a.getApplicationContext().getFilesDir().getAbsolutePath());

    try {
      mConfigPath.mkdirs();
      File file=new File(mConfigPath,filename);
      fos=new FileOutputStream(file);
      osw=new OutputStreamWriter(fos);
      osw.write(toString());
    } catch (Throwable t) { /* FilenotFound oder IOException */
      Log.e(TAG,"open/save. ",t);
    } finally {
      if(osw!=null) {
	try {osw.close();} catch (IOException e) {Log.e(TAG,"osw.close ",e);}
      }
      if(fos!=null) {
    	try {fos.close();} catch (IOException e) {Log.e(TAG,"fos.close ",e);}
      }
    }
  }
    
  public int load(Activity a,String filename) {
    int ret=Error.OK;
    InputStream in = null;
    Log.d(TAG,"read file: "+filename);

    try { 
      in = a.openFileInput(filename);
      if(in!=null) {
    	// prepare the file for reading
    	InputStreamReader inputreader = new InputStreamReader(in);
    	BufferedReader reader = new BufferedReader(inputreader);
    	String line;
    	try {line = reader.readLine();} 
	catch (IOException e) {return(-1);}

    	while (line!=null) {
    	  // do something with the settings from the file
    	  String[] sep=line.split("=");
    	  if(sep.length>1) {
	    try {
    	      if(sep[0].equalsIgnoreCase("num_data_conf"))  num_data_conf=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("interval"))  set_interval=(int)Double.parseDouble(sep[1]);

    	      else if(sep[0].equalsIgnoreCase("time_year")) time_year=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("time_mon"))  time_mon=(byte)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("time_mday")) time_mday=(byte)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("time_hour")) time_hour=(byte)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("time_min"))  time_min=(byte)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("time_sec"))  time_sec=(byte)Double.parseDouble(sep[1]);

    	      else if(sep[0].equalsIgnoreCase("starttime_year")) starttime_year=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("starttime_mon"))  starttime_mon=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("starttime_mday")) starttime_mday=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("starttime_hour")) starttime_hour=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("starttime_min"))  starttime_min=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("starttime_sec"))  starttime_sec=(int)Double.parseDouble(sep[1]);

    	      else if(sep[0].equalsIgnoreCase("stoptime_year")) stoptime_year=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("stoptime_mon"))  stoptime_mon=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("stoptime_mday")) stoptime_mday=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("stoptime_hour")) stoptime_hour=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("stoptime_min"))  stoptime_min=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("stoptime_sec"))  stoptime_sec=(int)Double.parseDouble(sep[1]);

    	      else if(sep[0].equalsIgnoreCase("temp_unit")) temp_unit=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("pres_unit")) pres_unit=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("led_conf"))  led_conf=(byte)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("start"))     start=(byte)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("startcondition")) startcondition=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("alarmtyp"))  alarmtyp=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("displaystate")) displaystate=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("date_format"))  date_format=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("time_format"))  time_format=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("pause"))        pause=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("language"))     language=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("stopcondition")) stopcondition=(int)Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("templimit_low"))  templimit_low=(float) Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("templimit_high")) templimit_high=(float) Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("humilimit_low"))  humilimit_low=(float) Double.parseDouble(sep[1]);
    	      else if(sep[0].equalsIgnoreCase("humilimit_high")) humilimit_high=(float) Double.parseDouble(sep[1]);
	      else if(sep[0].equalsIgnoreCase("preslimit_high")) preslimit_high=(float) Double.parseDouble(sep[1]);
	      else if(sep[0].equalsIgnoreCase("preslimit_low"))  preslimit_low=(float) Double.parseDouble(sep[1]);
	      else if(sep[0].equalsIgnoreCase("time_offset")) time_offset=(int) Double.parseDouble(sep[1]);
	      else if(sep[0].equalsIgnoreCase("time_delay"))  time_delay=(int) Double.parseDouble(sep[1]);
	      else if(sep[0].equalsIgnoreCase("flag_bits"))   flag_bits=(short) Double.parseDouble(sep[1]);
	      else if(sep[0].equalsIgnoreCase("name"))        setname(sep[1]);
	      else if(sep[0].equalsIgnoreCase("loggerid"))    loggerid=(int)Double.parseDouble(sep[1]);
	      else if(sep[0].equalsIgnoreCase("owner"))       Owner=sep[1];
    	      else if(sep[0].equalsIgnoreCase("location"))    Location=sep[1];
	      else if(sep[0].equalsIgnoreCase("report"))      Report=sep[1];
    	      else if(sep[0].equalsIgnoreCase("comment"))     Comment=sep[1];
	      else if(sep[0].equalsIgnoreCase("comment2"))    Comment2=sep[1];
	      else if(sep[0].equalsIgnoreCase("pdfname"))     Pdfname=sep[1];
	      else if(sep[0].equalsIgnoreCase("temp_offset")) temp_offset=(float) Double.parseDouble(sep[1]);
	      else if(sep[0].equalsIgnoreCase("humi_offset")) humi_offset=(float) Double.parseDouble(sep[1]);
	      else if(sep[0].equalsIgnoreCase("pres_offset")) pres_offset=(float) Double.parseDouble(sep[1]);
	      else Log.d(TAG,"Got line: "+line);
	    } catch (NumberFormatException e) {;}
    	  }
    	  try {line = reader.readLine();} 
	  catch (IOException e) {return Error.ERR;}
    	}
      }  else ret=Error.ERR;
      // close the file again
      try {in.close();} catch (IOException e) {return Error.ERR;}
    } catch (FileNotFoundException e) {
      Log.e(TAG,"ERROR: file not found ");
      return Error.ERR;
    } finally {
      if(in != null) {
    	try {in.close();} catch (IOException e) {return Error.ERR;}
      }
    }
    return(ret);
  }
}
