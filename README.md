# USB-Dataloggerreader for Android
<img alt="Logo" src="fastlane/metadata/android/en-US/images/icon.png" width="120" />


About USB-Dataloggerreader
==========================

(c) 2011-2020 by Markus Hoffmann

This app allows to use external USB data loggers together with your tablet 
computer or smart-phone.
It implements an interface to temperature, humidity, pressure, sound, 
voltage, current and or CO-gas concentrations USB data loggers. 
(It is supposed to run only on tablet computers and smart-phones, 
which have a USB-(Host)-connection/plug. Android versions >3.1)

It implements following functions:

1. Configuration of the logger,
2. readout of the configuration,
3. Start and Stop of the logging,
4. Readout of the measurement data,
5. Preview the data in a plot,
6. save the data into a file (.csv or ASCII).
7. examine the data in an interactive plot.

Following data logger devices are supported:

* Voltcraft DL-120-TH, DL-100-T
* Voltcraft DL-121-TH, DL-101-T, DL-181-THP
* Voltcraft DL-141-TH (experimental)
* Voltcraft DL-200T, DL-210TH, DL-220THP, DL-230L, DL-240K (new)
* CEM DT-171 (not tested)
* LOG32 
* Lascar EL-USB-1,2,3,4
* Lascar EL-USB-LITE,CO,TC,CO300,2-LCD,2+,1-PRO,TC-LCD,2-LCD+ (not tested)
* Lascar EL-USB-5,1-RCG,1-LCD,OEM-3 (not tested)
* USB500 Series
* FreeTec NC7004-675 black, KG100 (experimental)
* FreeTec NC7004-675 white, ecowitt DS102 (new)
* maybe others (not tested). 

### Download

[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/en/packages/com.drhoffmannstoolsdataloggerreader/)

### Screenshots

<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width="30%">
</div>
<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/5.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/6.png" width="30%">
</div>
<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/9.png" width="25%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/8.png" width="25%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/9.png" width="25%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/a.png" width="25%">
</div>

### Note:

There are currently issues with Android 10. 
Android 10 is not backwards compatible.
So some loggers are not detected anymore on Android 10. 
So better avoid upgrading until we have a fix.

### Important Note:

No software can be perfect. We do our best to keep this app bug free, 
improve it and fix all known errors as quick as possible. 
However, this program is distributed in the hope that it will 
be useful, but WITHOUT ANY WARRANTY; without even the implied 
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
Use this program on your own risk. Be aware, that all though unlikely, 
data loss of data stored in the data logger can occur in case 
of malfunction of the program or under special unforseen usage situations.
Please report all errors, so we can fix them. 

The WRITE EXTERNAL STORAGE permission is needed to write to the file system, 



    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; Version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.




Building the .apk file
======================

The build uses the gradle environment. A ./gradlew build should do.

 git clone git@codeberg.org:kollo/USBDataloggerreader-Android.git

then do a 
  cd USBDataloggerreader-Android
  ./gradlew build
(Enter passwords for the keystore)
(the compile process will take a while.)

The apk should finally be in build/outputs/apk/USBDataloggerreader-Android-release.apk

